﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class Dialog : MonoBehaviour
{
    private static Dialog instance;

    public GameObject DialogParents;
    public Transform Panel;

    private Confirm nowConfirmType;

    void Awake()
    {
        instance = this;
    }
    
    private static void AllDialogsExit()
    {
        for(int i = 0; i< instance.Panel.childCount; i++)
        {
            instance.Panel.GetChild(i).gameObject.SetActive(false);
        }
        instance.DialogParents.SetActive(false);
    }

    public static Transform ActiveDialog(string name)
    {
        instance.DialogParents.SetActive(true);
        Transform ret = instance.Panel.Find(name);
        ret.gameObject.SetActive(true);
        return ret;
    }

    public static void SetChatBotCount(int count)
    {
        Transform dialog = GetDialog("Bot");
        dialog.Find("NowChattingBotNumber").GetComponent<Text>().text = "( " + count + " / 20 )";
    }

    private static Transform GetDialog(string name)
    {
        return instance.Panel.Find(name);
    }

    

    #region RequestDialogs

    public static void RequestConfirmDialog(Confirm type)
    {
        instance.nowConfirmType = type;
        Transform dialog = ActiveDialog("Confirm");
        string warning = "경고";
        switch (type)
        {
            case Confirm.DeleteUser:
                warning = "정말 유저정보를 지우시겠습니까? 복구가 불가합니다.";
                break;
            case Confirm.ClientAllLogout:
                warning = "모든 유저를 로그아웃 시킵니다.";
                break;
            case Confirm.DeleteDeposit:
            case Confirm.DeleteWithdraw:
                warning = "지우시겠습니까?";
                break;
        }
        dialog.Find("Warning").GetComponent<Text>().text = warning;
    }

    public void RequestJacpot()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;
        Transform dialog = ActiveDialog("Jacpot");
        dialog.Find("Id").GetComponent<Text>().text = id;

        dialog.Find("Server").GetComponent<Text>().text = Tool.GetServer(index);
    }

    //
    public void RequestExample()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);

        Transform dialog = ActiveDialog("Example");
        dialog.Find("Id").GetComponent<Text>().text = id;
        dialog.Find("NowExample").GetComponent<Text>().text = Tool.GetExample();
        
    }
    
    //현재홀드상태 불러오는거 해야함.
    public void RequestHold()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;
        Transform dialog = ActiveDialog("Hold");
        dialog.Find("Id").GetComponent<Text>().text = id;
        dialog.Find("NowState").GetComponent<Text>().text = Tool.GetHoldState();
    }

    public void RequestExchange()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;
        
        Transform dialog = ActiveDialog("ExchangeHold");
        dialog.Find("Id").GetComponent<Text>().text = id;
        dialog.Find("GiftExchange").GetComponent<Text>().text = Tool.GetGiftExchange();
        dialog.Find("MoneyExchange").GetComponent<Text>().text = Tool.GetMoneyExchange();
        
    }

    public void RequestBot()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);


        if (!Tool.IsBot() && id != " ")
            return;
        
        Transform dialog = ActiveDialog("Bot");
        dialog.Find("InputBotNameForDelete").GetComponent<InputField>().text = id;
        dialog.Find("NowServer").GetComponent<Text>().text = Tool.GetServer();
        SocketManager.Send("1#!13");
    }

    public void RequestOut()
    {
        int index = Tool.GetSelectedIndex();

        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;
        
        Transform dialog = ActiveDialog("Block");

        dialog.Find("Id").GetComponent<Text>().text = id;


    }

    public void RequestBank()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;

        
        Transform dialog = ActiveDialog("Bank");

        dialog.Find("Id").GetComponent<Text>().text = id;
        dialog.Find("Credit").GetComponent<Text>().text = Tool.GetMoney();
        
    }

    public void RequestNotice()
    {
        Transform dialog = ActiveDialog("Notice");

        int index = Tool.GetSelectedIndex();
        if (index == -1)
            dialog.Find("Id").GetComponent<Text>().text = "전체공지";

        

        string id = Tool.GetID();
        if (id == " ")
        {
            dialog.Find("Id").GetComponent<Text>().text = "전체공지";
        }
        else
            dialog.Find("Id").GetComponent<Text>().text = id;
    }

    public void RequestReserve()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        
        Transform dialog = ActiveDialog("Reserve");

        string id = Tool.GetID();
        //ID
        if (id != " ")
        {
            InputField inputId = dialog.Find("InputID").GetComponent<InputField>();
            inputId.text = id;
            inputId.readOnly = true;
        }
        else
        {
            InputField inputId = dialog.Find("InputID").GetComponent<InputField>();
            inputId.text = "";
            inputId.readOnly = false;
        }
        //서버 - 기계번호
        dialog.Find("Machine").GetComponent<Text>().text = ""+Tool.GetMachine();
        dialog.Find("Server").GetComponent<Text>().text = "" + Tool.GetServer();

        //남은시간
        string remainTime = Tool.GetReserve();
        dialog.Find("RemainTime").GetComponent<Text>().text = remainTime;
    }

    public void RequestUserInfo()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;
        if (Tool.IsBot())
            return;
        
        Transform dialog = ActiveDialog("UserInfo");

        dialog.Find("Id").GetComponent<Text>().text = id;
        SocketManager.Send("1#!11#!" + id);
    }

    public void RequestSpin()
    {
        int index = Tool.GetSelectedIndex();
        if (index == -1)
            return;
        string id = Tool.GetID(index);
        if (id == " ")
            return;


        Transform dialog = ActiveDialog("Spin");

        dialog.Find("Id").GetComponent<Text>().text = id;
        dialog.Find("Credit").GetComponent<Text>().text = Tool.GetSpin();
    }

    public static void RequestUserInfo(string id)
    {
        Transform dialog = ActiveDialog("UserInfo");
          
        dialog.Find("Id").GetComponent<Text>().text = id;
        SocketManager.Send("1#!11#!" + id);
    }

    public void RequestUserInfo(InputField text)
    {
        Transform dialog = ActiveDialog("UserInfo");

        dialog.Find("Id").GetComponent<Text>().text = text.text;
        SocketManager.Send("1#!11#!" + text.text);
    }

    public static void RequestReserveForTable()
    {

        int index = Reserve.GetSelectedIndex();
        if (index == -1)
            return;

        Transform dialog = ActiveDialog("Reserve");

        string id = Reserve.GetSelectedUserID();
        //ID
        InputField inputId = dialog.Find("InputID").GetComponent<InputField>();
        inputId.text = id;
        inputId.readOnly = true;
        
        
        //서버 - 기계번호
        dialog.Find("Machine").GetComponent<Text>().text = "" + Reserve.GetMachine();
        dialog.Find("Server").GetComponent<Text>().text = "" + Reserve.GetServer();

        //남은시간
        string remainTime = Tool.GetReserve();
        dialog.Find("RemainTime").GetComponent<Text>().text = remainTime;
        
    }

    public static void RequestWarning(string warning)
    {
        Transform dialog = ActiveDialog("Warning");
        dialog.Find("WarnText").GetComponent<Text>().text = warning;
    }

    public static void RequestWaiting(string waiting)
    {
        Transform dialog = ActiveDialog("Waiting");
        dialog.Find("Text").GetComponent<Text>().text = waiting;
    }

    public static void ExitDialogWaiting()
    {
        AllDialogsExit();
        instance.DialogParents.SetActive(false);
    }
    
    public static void RequestAlarmInfo(string info)
    {
        Transform dialog = ActiveDialog("AlarmInfo");
        dialog.Find("Text").GetComponent<Text>().text = info;
    }

    #endregion



    #region Events

    public void ExitDialog()
    {
        AllDialogsExit();
    }

    public void ConfirmOK()
    {
        switch(nowConfirmType)
        {
            case Confirm.DeleteUser:
                SocketManager.Send("2#!2#!" + UserInfo.GetSelectedUserID());
                break;
            case Confirm.ClientAllLogout:
                SocketManager.Send("2#!23");
                break;
            case Confirm.DeleteDeposit:
                Deposit.RequestDeleteNowIndex();
                break;
            case Confirm.DeleteWithdraw:
                Withdraw.RequestDeleteNowIndex();
                break;
        }
        ExitDialog();
    }

    public void SendJacpot()
    {
        Transform dialog = GetDialog("Jacpot");
        int drop = dialog.Find("Dropdown").GetComponent<Dropdown>().value;
        if (drop > 0)
            drop = Int32.Parse(dialog.Find("Dropdown").GetComponent<Dropdown>().options[drop].text );
        //inputMoney를 돌발로 바꾼다.
        string dolbalStr = dialog.Find("InputMoney").GetComponent<InputField>().text;
        string divisionStr = dialog.Find("InputDivision").GetComponent<InputField>().text;
        int dolbal=0, division=0;
        if (dolbalStr != "")
            dolbal = Int32.Parse(dolbalStr);
        if (divisionStr!= "")
            division = Int32.Parse(divisionStr);

        int check = 0;
        if (drop > 0) check += 1;
        if (dolbal > 0) check += 1;
        if (check > 1 || check == 0)
        {
            dialog.Find("Warning").GetComponent<Text>().text = "돌발, 총금액 중 하나만 선택해야합니다.";
            return;
        }


        if (drop > 0)
        {
            if (division == 0)
            {
                dialog.Find("Warning").GetComponent<Text>().text = "분할금액을 확인하세요.";
                return;
            }
            else
            {
                SocketManager.Send("1#!1#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!3#!" + drop * 10000 + "#!" + division * 10000);
                Logs.AddLog("[Tool]잭팟: " + Tool.GetServer() + "/" + Tool.GetMachine() + "  금액:" + drop + "만원 / 분할:" + division * 10000);
            }
        }
        else if (dolbal > 0)
        {
            SocketManager.Send("1#!14#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!" + dolbal*10000);
            Logs.AddLog("[Tool]돌발: " + Tool.GetServer() + "/" + Tool.GetMachine() + "  금액:" + dolbal* 10000);

        }
        
        ExitDialog();
    }

    public void SendJacpotDelete()
    {
        Transform dialog = GetDialog("Jacpot");
       
        
        SocketManager.Send("1#!1#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!2#!" + 0);
        Logs.AddLog("[Tool]잭팟삭제: " + Tool.GetServer() + "/" + Tool.GetMachine());

        ExitDialog();
    }

    public void SendExample()
    {
        Transform dialog = GetDialog("Example");
        int drop = dialog.Find("Dropdown").GetComponent<Dropdown>().value;
        drop += 1;
        
        SocketManager.Send("1#!2#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!"+drop);
        ExitDialog();
        Logs.AddLog("[Tool]예시 :" + Tool.GetServer() + "/" + Tool.GetMachine()+ " 예시:"+drop);

    }

    public void SendExampleDelete()
    {
        SocketManager.Send("1#!2#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!" + 0);
        ExitDialog();
        Logs.AddLog("[Tool]예시삭제 :" + Tool.GetServer() + "/" + Tool.GetMachine());

    }

    public void SendHold()
    {
        SocketManager.Send("1#!3#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!" + 1);
        ExitDialog();
        Logs.AddLog("[Tool]홀드 :" + Tool.GetServer() + "/" + Tool.GetMachine());

    }

    public void SendCancelHold()
    {
        SocketManager.Send("1#!3#!" + Tool.GetServer() + "#!" + Tool.GetMachine() + "#!" + 0);
        ExitDialog();
        Logs.AddLog("[Tool]홀드취소 :" + Tool.GetServer() + "/" + Tool.GetMachine());
    }

    public void SendExchange()
    {

        Transform dialog = GetDialog("ExchangeHold");
        int gift = dialog.Find("DropdownGift").GetComponent<Dropdown>().value;
        int money = dialog.Find("DropdownMoney").GetComponent<Dropdown>().value;
        SocketManager.Send("1#!4#!" + Tool.GetID() + "#!" + gift+ "#!" + money);
        ExitDialog();

        Logs.AddLog("[Tool]전환불가설정:"+Tool.GetID()+"gift:"+gift+"  money:"+money);
    }

    public void SendBot()
    {
        Transform dialog = GetDialog("Bot");
        string botName = dialog.Find("InputBotName").GetComponent<InputField>().text;
        bool canChat = dialog.Find("Toggle").GetComponent<Toggle>().isOn;
        if (canChat)
            SocketManager.Send("1#!5#!" + botName + "#!1#!"+Tool.GetServer()+"#!"+Tool.GetMachine());
        else
            SocketManager.Send("1#!5#!" + botName + "#!0#!" + Tool.GetServer() + "#!" + Tool.GetMachine());
        ExitDialog();
        Logs.AddLog("[Tool]봇추가:" + botName);
    }


    public void SendBotDelete()
    {
        Transform dialog = GetDialog("Bot");
        string botName = dialog.Find("InputBotNameForDelete").GetComponent<InputField>().text;

        SocketManager.Send("1#!5#!" + botName + "#!2");
        ExitDialog();
        Logs.AddLog("[Tool]봇삭제:" + botName);
    }

    public void SendBotRandom()
    {
        Transform dialog = GetDialog("Bot");
        int botCount = Int32.Parse(dialog.Find("InputNumForRandom").GetComponent<InputField>().text);
        if (botCount < 1 && botCount > 60)
            return;

        SocketManager.Send("1#!5#!#!3#!"+Tool.GetServer() +"#!"+botCount);
        ExitDialog();
        Logs.AddLog("[Tool]봇 " +botCount+"개 랜덤추가, "+Tool.GetServer()+"Server");
    }

    public void SendOut()
    {
        Transform dialog = GetDialog("Block");
        string id = dialog.Find("Id").GetComponent<Text>().text;
        
        SocketManager.Send("1#!6#!"+id+"#!0");
        ExitDialog();
        Logs.AddLog("[Tool]내보내기 ID:" + id);
    }

    public void SendBlock()
    {
        Transform dialog = GetDialog("Block");
        string id = dialog.Find("Id").GetComponent<Text>().text;
        string reason = dialog.Find("InputReason").GetComponent<InputField>().text;
        SocketManager.Send("1#!6#!" + id + "#!1#!"+reason);
        ExitDialog();
        Logs.AddLog("[Tool]차단 ID:" + id + "  이유:" + reason);
    }

    /// <summary>
    /// 0입금 1출금
    /// </summary>
    /// <param name="bank"></param>
    public void SendBank(int bank)
    {
        Transform dialog = GetDialog("Bank");
        string pushMoney = dialog.Find("InputSendMoney").GetComponent<InputField>().text;
        string stealMoney = dialog.Find("InputRecvMoney").GetComponent<InputField>().text;
        string id = Tool.GetID();

        

        if (bank == 0)
        {
            if (pushMoney == "")
                return;
            SocketManager.Send("1#!7#!" +id+ "#!0#!" + pushMoney);
            Logs.AddLog("[Tool]강제입출:" + id + "유저에게 " + pushMoney + "원 강제입금");
        } else if(bank == 1)
        {
            if (stealMoney == "")
                return;
            SocketManager.Send("1#!7#!" +id+ "#!1#!" + stealMoney);
            Logs.AddLog("[Tool]강제입출:" + id + "유저에게 " + stealMoney+ "원 강제출금");

        }
        ExitDialog();
        
    }

    public void SetAllNotice()
    {
        
        Transform dialog = GetDialog("Notice");

        dialog.Find("Id").GetComponent<Text>().text = "전체공지";
    }

    public void SendNotice()
    {
        Transform dialog = GetDialog("Notice");
        string ID = dialog.Find("Id").GetComponent<Text>().text;
        string Notice = dialog.Find("InputNotice").GetComponent<InputField>().text;
        Notice = Set1000Char(Notice);
        SocketManager.Send("1#!8#!" + ID + "#!" + Notice);
        ExitDialog();
        Logs.AddLog("[Tool]공지사항("+ID+"):" + Notice);
    }

    public void SendReserve()
    {
        //1  9 S M ID Time

        Transform dialog = GetDialog("Reserve");

        string ID = dialog.Find("InputID").GetComponent<InputField>().text;
        string server = dialog.Find("Server").GetComponent<Text>().text;
        string machine = dialog.Find("Machine").GetComponent<Text>().text;
        int reserveHour = dialog.Find("Dropdown").GetComponent<Dropdown>().value;
        string reserveHourWrite = dialog.Find("InputTime").GetComponent<InputField>().text;
        int reserveWrite = 0;

        if (reserveHourWrite == "" || reserveHourWrite == " ")
            reserveWrite = 0;
        else
            reserveWrite = Int32.Parse(reserveHourWrite);

        if (reserveHour != 0 && reserveWrite != 0)
            return;
        int sendHour = reserveWrite + reserveHour;

        SocketManager.Send("1#!9#!" + server + "#!" + machine + "#!" + ID + "#!" + sendHour);
        ExitDialog();
        Logs.AddLog("[Tool]ID:"+ID + "   " + server + "서버 " + machine + "머신 "+sendHour+"시간 예약");
    }

    public void SendReserveDelete()
    {
        Transform dialog = GetDialog("Reserve");

        string ID = dialog.Find("InputID").GetComponent<InputField>().text;
        string server = dialog.Find("Server").GetComponent<Text>().text;
        string machine = dialog.Find("Machine").GetComponent<Text>().text;

        SocketManager.Send("1#!9#!" + server + "#!" + machine + "#!" + ID + "#!" + 0);
        ExitDialog();
        Logs.AddLog("[Tool]ID:" + ID + "   " + server + "서버 " + machine + "머신 예약해제");
    }
    
    //1 12 ID PW MEMO
    public void SendSetUserInfo()
    {
        Transform dialog = GetDialog("UserInfo");

        string id = dialog.Find("Id").GetComponent<Text>().text;
        string PW = dialog.Find("InputPw").GetComponent<InputField>().text;
        string Memo = dialog.Find("InputMemo").GetComponent<InputField>().text;
        int grade = dialog.Find("DropdownGrade").GetComponent<Dropdown>().value;

        SocketManager.Send("1#!12#!" + id + "#!" + PW+"#!"+ grade + "#!" + Memo);

        ExitDialog();
        Logs.AddLog("[Tool] "+id+" 유저정보 수정  PW:" + PW + " Memo:" + Memo);
    }

    public void SendSpin(int spin)
    {
        Transform dialog = GetDialog("Spin");
        string pushSpin = dialog.Find("InputSendMoney").GetComponent<InputField>().text;
        string stealSpin = dialog.Find("InputRecvMoney").GetComponent<InputField>().text;
        string id = Tool.GetID();



        if (spin == 0)
        {
            if (pushSpin == "")
                return;
            SocketManager.Send("1#!15#!" + id + "#!0#!" + pushSpin);
            Logs.AddLog("[Tool]Spin가산:" + id + "유저에게 " + pushSpin + "개 강제 가산");
        }
        else if (spin == 1)
        {
            if (stealSpin == "")
                return;
            SocketManager.Send("1#!15#!" + id + "#!1#!" + stealSpin);
            Logs.AddLog("[Tool]Spin차감:" + id + "유저에게 " + stealSpin + "원 강제 차감");

        }
        ExitDialog();

    }

    private static string Set1000Char(string str)
    {
        if (str.Length > 1000)
        {
            return str.Substring(0, 1000);
        }
        return str;
    }


    #endregion

    #region ChangedValues

    public void JacpotYuntaCalc()
    {
        Transform dialog = GetDialog("Jacpot");
        Text calcText = dialog.Find("Calc").GetComponent<Text>();
        string division = dialog.Find("InputDivision").GetComponent<InputField>().text;
        int drop = dialog.Find("Dropdown").GetComponent<Dropdown>().value;
        if (drop > 0)
            drop = Int32.Parse(dialog.Find("Dropdown").GetComponent<Dropdown>().options[drop].text);
        else
            return;

        int div = 0;
        int yun = 0;

        if (division == "")
        {
            calcText.text = "";
            return;
        }
        else
            Int32.TryParse(division , out div);

        if (drop % div != 0)
        {
            dialog.Find("Warning").GetComponent<Text>().text = "금액을 확인하세요";
            return;
        }
        else
        {
            yun = drop / div;
            dialog.Find("Warning").GetComponent<Text>().text = "";
        }

        calcText.text = "분할 "+division+ "만원 x "+ yun +"연타= 총금액 "+drop+"만원" ;
    }

    #endregion
}

enum Confirm
{
    DeleteUser, ClientAllLogout, DeleteDeposit, DeleteWithdraw
}
