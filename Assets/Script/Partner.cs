﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Partner : MonoBehaviour {
    private static Partner instance;


    public GameObject Table; //RowGroup
    public GameObject RowPrefab;
    public InputField IF_ID, IF_PW, IF_Rate, IF_Code;
    private List<GameObject> RowList = new List<GameObject>();

    public int selectListIndex;

    void Awake()
    {
        instance = this;
    }

    public static void AddRow(string id, string pw, string rate, string code, string usernum)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.Table.transform);

            instance.RowList.Add(item);
            SetOnClick(instance.RowList.Count - 1);

        }
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }


        SetItem(item, "Id", "" + id);
        SetItem(item, "Pw", "" + pw);
        SetItem(item, "Rate", "" + rate);
        SetItem(item, "Code", "" + code);
        SetItem(item, "UserNum", "" + usernum);

    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
    }

    /*******************************************************************************************************/

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.RowList.Count > index)
        {
            instance.RowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            if (!instance.RowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnClick(int index)
    {
        instance.RowList[index].GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index;  ClickModule(index);   });
    }

    private static string GetItem(GameObject row, string name)
    {
        return row.transform.Find(name).GetComponent<Text>().text;
    }

    private static void  ClickModule(int index)
    {
        instance.IF_ID.text = GetItem(instance.RowList[index], "Id");
        instance.IF_Rate.text = GetItem(instance.RowList[index], "Rate");
        instance.IF_PW.text = GetItem(instance.RowList[index], "Pw");
        instance.IF_Code.text = GetItem(instance.RowList[index], "Code");

    }

    public void ClickAddPartner()
    {
        if (IF_Code.text == "" || IF_ID.text == "" || IF_PW.text == "" || IF_Rate.text == "")
            return;
        SocketManager.Send("2#!19#!" + IF_ID.text + "#!" + IF_PW.text + "#!" + IF_Rate.text + "#!" + IF_Code.text);

        Dialog.RequestWaiting("파트너 등록중.\n 잠시만 기다려주십시오.");
        StartCoroutine(AddPartnerRefresh());
    }

    IEnumerator AddPartnerRefresh()
    {
        yield return new WaitForSeconds(1.0f);
        Dialog.ExitDialogWaiting();
        SocketManager.Send("2#!18");
        Dialog.RequestAlarmInfo("파트너 등록 완료");
    }
}
