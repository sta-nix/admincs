﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class Alarm : MonoBehaviour
{
    private static Alarm instance;

    private AudioSource audioSrc;
    private bool isMute = false;

    public AudioClip clip;

    public GameObject UserAlarm;
    public Text UserAlarmTitle;
    public Text UserAlarmContent;

    public Text depositNumber;
    public Text withdrawNumber;
    public Text questionNumber;

    public Text depositText;
    public Text withdrawText;
    public Text questionText;

    public Text server100Number;
    public  Text server300Number;
    public  Text server500Number;
    public  Text server1000Number;
    public  Text onlineNumber;

    public  Text chattingNumber;     //확인안한 채팅의수
    public Text mooneNumber;
    public Text userNumber;         //가입된 유저 총
    public Text adjustmentNumber;   //입금출금 처리안된 총 횟수(알람 ㄴㄴ)
    public Text partnerNumber;      //가입된 파트너 총

    public Image alarmImage;        //소리나며 깜빡일넘..

    public static Color defaultColor = new Color(0.37f, 0.41f, 0.44f, 1);
   
    private bool isDepositAlarm = false, isWithdrawAlarm = false, isQuestionAlarm = false, isUserAlarm;

    private IEnumerator depositEnumerator, withdrawEnumerator, questionEnumerator;

    //리스트에 번호가 있으면 계속 알람이 울린담..
    //LPanel이 리셋 될때마다 이것도 리셋시켜야할듯.. 아.. 아닌가?
    //처음 LPanel이 올때 알람이 설정이 안되네..
    // 아 그러면.. 뭐가 됬다고 보내지말고.. 그냥 전체 상태를 보내는게 어떨까 싶음.
    // 개수- 소리남/안남 이렇게.
    private List<int> depositAlarmList = new List<int>();
    private List<int> withdrawAlarmList = new List<int>();

    void Awake()
    {
        instance = this;
        AudioSrc = gameObject.AddComponent<AudioSource>();
        AudioSrc.loop = true;
        AudioSrc.clip = clip;
    }

    void Start()
    {
        DepositEnumerator = DepositTextAlarm();
        WithdrawEnumerator = WithdrawTextAlarm();
        QuestionEnumerator = QuestionTextAlarm();

        //StartDepositAlarm();
        //StartWithdrawAlarm();
        //StartQuestionAlarm();
    }

    private IEnumerator DepositTextAlarm()
    {
        WaitForSeconds waitSec = new WaitForSeconds(0.5f);

        DepositText.color = Color.white;
        while (true)
        {
            DepositText.CrossFadeColor(new Color(1,1,1,1), 0.1f, false, true);
            yield return waitSec;
            DepositText.CrossFadeColor(defaultColor, 0.1f, false, false);
            yield return waitSec;
        }
    }

    private IEnumerator WithdrawTextAlarm()
    {
        WaitForSeconds waitSec = new WaitForSeconds(0.5f);

        WithdrawText.color = Color.white;
        while (true)
        {

            WithdrawText.CrossFadeColor(new Color(1, 1, 1, 1), 0.1f, false, true);
            yield return waitSec;
            WithdrawText.CrossFadeColor(defaultColor, 0.1f, false, false);
            yield return waitSec;
        }
    }
    private IEnumerator QuestionTextAlarm()
    {
        WaitForSeconds waitSec = new WaitForSeconds(0.5f);

        QuestionText.color = Color.white;
        while (true)
        {
            QuestionText.CrossFadeColor(new Color(1, 1, 1, 1), 0.1f, false, true);
            yield return waitSec;
            QuestionText.CrossFadeColor(defaultColor, 0.1f, false, false);
            yield return waitSec;
        }
    }
    

    //문의 알람을 처리한다.
    public static void AlarmQuestion()
    {
        Add(QuestionNumber, 1);
        StartQuestionAlarm();
    }

    //새로운 채팅이 들어올때마다 호출되는데... 문의가 아닌이상 별기능 없음.
    public static void AlarmChat()
    {
        Add(ChattingNumber, 1);
    }

    public static void StartDepositAlarm()
    {
        if (IsDepositAlarm)
            return;
        IsDepositAlarm = true;
        SetAlarmSoundState();
        instance.StartCoroutine(DepositEnumerator);
    }

    public static void StopDepositAlarm()
    {
        IsDepositAlarm = false;
        SetAlarmSoundState();
        instance.StopCoroutine(DepositEnumerator);
        DepositText.color = Color.white;
        DepositText.CrossFadeColor(defaultColor, 1, false, false);
    }

    public static void StartWithdrawAlarm()
    {
        if (IsWithdrawAlarm)
            return;
        IsWithdrawAlarm = true;
        SetAlarmSoundState();
        instance.StartCoroutine(WithdrawEnumerator);
    }

    public static void StopWithdrawAlarm()
    {
        IsWithdrawAlarm = false;
        SetAlarmSoundState();
        instance.StopCoroutine(WithdrawEnumerator);
        WithdrawText.color = Color.white;
        WithdrawText.CrossFadeColor(defaultColor, 1, false, false);
    }

    public static void StartQuestionAlarm()
    {
        if (IsQuestionAlarm)
            return;
        IsQuestionAlarm = true;
        SetAlarmSoundState();
        instance.StartCoroutine(QuestionEnumerator);
    }

    public static void StopQuestionAlarm()
    {
        IsQuestionAlarm = false;
        SetAlarmSoundState();
        instance.StopCoroutine(QuestionEnumerator);
        QuestionText.color = Color.white;
        QuestionText.CrossFadeColor(defaultColor, 1, false, false);
    }

    //여따가 다 넣음..
    public static void StartUserAlarm(string id)
    {
        IsUserAlarm = true;
        SetAlarmSoundState();
        instance.UserAlarm.SetActive(true);
        instance.UserAlarmContent.text = "ID : " + id;
    }

    public static void StopUserAlarm()
    {
        IsUserAlarm = false;
        SetAlarmSoundState();
    }


    private static void SetAlarmSoundState()
    {
        if (IsDepositAlarm || IsQuestionAlarm || IsWithdrawAlarm || IsUserAlarm)
        {
            if (!AudioSrc.isPlaying && !IsMute)
                AudioSrc.Play();
        }
        else
        {
            if (AudioSrc.isPlaying)
                AudioSrc.Stop();
        }

    }

    public static void AddServer(string server, int count)
    {
        switch(server)
        {
            case "100":
                Add(Server100Number, count);
                break;
            case "300":
                Add(Server300Number, count);
                break;
            case "500":
                Add(Server500Number, count);
                break;
            case "1000":
                Add(Server1000Number, count);
                break;
        }
    }

    public static void Add(Text textComponent, int count)
    {
        int number = Int32.Parse(textComponent.text);
        textComponent.text = number + count + "";
    }

    public static void Set(Text textComponent, int count)
    {
        textComponent.text = ""+count;
    }

    public void EndAlert()
    {
        StopDepositAlarm();
        StopQuestionAlarm();
        StopWithdrawAlarm();
    }

    #region Events

    public void ExitUserAlarm()
    {
        StopUserAlarm();
        UserAlarm.SetActive(false);
    }

#endregion

    #region Setter Getter

    public static Text DepositNumber
    {
        get
        {
            return instance.depositNumber;
        }

        set
        {
            instance.depositNumber = value;
        }
    }

    public static Text WithdrawNumber
    {
        get
        {
            return instance.withdrawNumber;
        }

        set
        {
            instance.withdrawNumber = value;
        }
    }

    public static Text QuestionNumber
    {
        get
        {
            return instance.questionNumber;
        }

        set
        {
            instance.questionNumber = value;
        }
    }

    public static Text DepositText
    {
        get
        {
            return instance.depositText;
        }

        set
        {
            instance.depositText = value;
        }
    }

    public static Text WithdrawText
    {
        get
        {
            return instance.withdrawText;
        }

        set
        {
            instance.withdrawText = value;
        }
    }

    public static Text QuestionText
    {
        get
        {
            return instance.questionText;
        }

        set
        {
            instance.questionText = value;
        }
    }

    public static Text Server100Number
    {
        get
        {
            return instance.server100Number;
        }

        set
        {
            instance.server100Number = value;
        }
    }

    public static Text Server300Number
    {
        get
        {
            return instance.server300Number;
        }

        set
        {
            instance.server300Number = value;
        }
    }

    public static Text Server500Number
    {
        get
        {
            return instance.server500Number;
        }

        set
        {
            instance.server500Number = value;
        }
    }

    public static Text Server1000Number
    {
        get
        {
            return instance.server1000Number;
        }

        set
        {
            instance.server1000Number = value;
        }
    }

    public static Text OnlineNumber
    {
        get
        {
            return instance.onlineNumber;
        }

        set
        {
            instance.onlineNumber = value;
        }
    }

    public static Text ChattingNumber
    {
        get
        {
            return instance.chattingNumber;
        }

        set
        {
            instance.chattingNumber = value;
        }
    }

    public static Text UserNumber
    {
        get
        {
            return instance.userNumber;
        }

        set
        {
            instance.userNumber = value;
        }
    }

    public static Text AdjustmentNumber
    {
        get
        {
            return instance.adjustmentNumber;
        }

        set
        {
            instance.adjustmentNumber = value;
        }
    }

    public static Text PartnerNumber
    {
        get
        {
            return instance.partnerNumber;
        }

        set
        {
            instance.partnerNumber = value;
        }
    }

    public static Image AlarmIamge
    {
        get
        {
            return instance.alarmImage;
        }

        set
        {
            instance.alarmImage = value;
        }
    }

    public static bool IsDepositAlarm
    {
        get
        {
            return instance.isDepositAlarm;
        }

        set
        {
            instance.isDepositAlarm = value;
        }
    }

    public static bool IsWithdrawAlarm
    {
        get
        {
            return instance.isWithdrawAlarm;
        }

        set
        {
            instance.isWithdrawAlarm = value;
        }
    }

    public static bool IsQuestionAlarm
    {
        get
        {
            return instance.isQuestionAlarm;
        }

        set
        {
            instance.isQuestionAlarm = value;
        }
    }

    public static bool IsUserAlarm
    {
        get
        {
            return instance.isUserAlarm;
        }

        set
        {
            instance.isUserAlarm = value;
        }
    }

    public static AudioSource AudioSrc
    {
        get
        {
            return instance.audioSrc;
        }

        set
        {
            instance.audioSrc = value;
        }
    }

    public static IEnumerator DepositEnumerator
    {
        get
        {
            return instance.depositEnumerator;
        }

        set
        {
            instance.depositEnumerator = value;
        }
    }

    public static IEnumerator WithdrawEnumerator
    {
        get
        {
            return instance.withdrawEnumerator;
        }

        set
        {
            instance.withdrawEnumerator = value;
        }
    }

    public static IEnumerator QuestionEnumerator
    {
        get
        {
            return instance.questionEnumerator;
        }

        set
        {
            instance.questionEnumerator = value;
        }
    }

    public static Text MooneNumber
    {
        get
        {
            return instance.mooneNumber;
        }

        set
        {
            instance.mooneNumber = value;
        }
    }

    public static bool IsMute
    {
        get
        {
            return instance.isMute;
        }

        set
        {
            instance.isMute = value;
        }
    }

    #endregion
}

