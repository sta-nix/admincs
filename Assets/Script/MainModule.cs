﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Timers;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainModule : MonoBehaviour {

    private static MainModule instance;

    public GameObject LoginObject;
    public GameObject LeftObject;
    public GameObject MainObject;
    public GameObject PartnerPrivateObject;

    public GameObject ToolObject, ChatObject;
    public GameObject UserObject, AdjustmentObject;
    public GameObject PartnerObject,QuestionObject, ManagerObject;

    public GameObject UserInfosObject, ReserveInfoObject, BanInfoObject;

    public GameObject RevenueListObject, DepositListObject, WithdrawListObject, VirtualListObject;
    public GameObject PartnerListObject, PartnerCalculateObject;

    private Queue<string> workQueue = new Queue<string>();

    private Page NowPage = Page.None;

    private static System.Timers.Timer RefreshTimer;
    private int refreshTemp = 0;

    void Awake()
    {
        instance = this;
        RefreshTimer = new System.Timers.Timer(1000);
        RefreshTimer.Elapsed += Refresh;
        RefreshTimer.Enabled = true;
    }



    // Update is called once per frame
    void Update() {
        if (workQueue.Count > 0)
        {
            RunRecvModule(workQueue.Dequeue());
        }
    }

    public static void RecvModule(string msg)
    {
        instance.workQueue.Enqueue(msg);
    }

    private static string RunRecvModule(string msg)
    {
        string ret = "0";
        string index = msg;
        if (msg.Length > 11)
            index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(index, "#!");
        switch ((RecvType)Int32.Parse(msgs[0]))
        {
            case RecvType.Tool:
                ret = RecvToolModule(msg);
                break;
            case RecvType.Manage:
                ret = RecvManageModule(msg);
                break;
            case RecvType.Chat:
                ret = RecvChatModule(msg);
                break;
            case RecvType.Alarm:
                ret = RecvAlarmModule(msg);
                break;
            case RecvType.Login:
                ret = RecvLoginModule(msg);
                break;
            case RecvType.Partner:
                ret = RecvPartnerModule(msg);
                break;
        }
        return ret;
    }



   
    private static string RecvToolModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(index, "#!");

        switch ((RecvTool)Int32.Parse(msgs[1]))
        {
            case RecvTool.JacpotServer:
                JacpotServer(msg);
                break;
            case RecvTool.GetServer:
                GetServer(msg);
                break;
            case RecvTool.GetUserInfo:
                GetUserInfo(msg);
                break;
            case RecvTool.GetChatBotCount:
                GetChatBotCount(msg);
                break;
        }
        return ret;
    }



    private static string RecvManageModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(msg, "#!");
        switch ((RecvManage)Int32.Parse(msgs[1]))
        {
            case RecvManage.GetUserList:
                SetUserList(msg);
                break;
            case RecvManage.GetJoinUser:
                GetJoinUserCount(msg);
                break;
            case RecvManage.GetReserveList:
                GetReserveList(msg);
                break;
            case RecvManage.GetBanList:
                GetBanList(msg);
                break;
            case RecvManage.GetRevenueList:
                GetRevenueList(msg);
                break;
            case RecvManage.GetPartnerListRate:
                GetPartnerListRate(msg);
                break;
            case RecvManage.GetDepositList:
                GetDepositList(msg);
                break;
            case RecvManage.GetWithdrawList:
                GetWithdrawList(msg);
                break;
            case RecvManage.DepositConfirmOK:
                DepositConfirmOK(msg);
                break;
            case RecvManage.WithdrawConfirmOK:
                WithdrawConfirmOK(msg);
                break;
            case RecvManage.GetVirtualList:
                GetVirtualList(msg);
                break;
            case RecvManage.GetPartnerList:
                GetPartnerList(msg);
                break;
            case RecvManage.GetCalculateList:
                GetCalculateList(msg);
                break;
            case RecvManage.GetManagerInfo:
                GetManagerInfo(msg);
                break;
            case RecvManage.GetQuestionList:
                GetQuestionList(msg);
                break;
        }
        return ret;
    }

    

    private static string RecvChatModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(msg, "#!");
        switch ((RecvChat)Int32.Parse(msgs[1]))
        {
            case RecvChat.GetBotNUserList:
                GetBotNUserList(msg);
                break;
            case RecvChat.GetChatLogList:
                GetChatLogList(msg);
                break;
            case RecvChat.GetChat:
                GetChatLog(msg);
                break;
        }
        return ret;
    }

    private static string RecvAlarmModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(msg, "#!");
        switch ((RecvAlarm)Int32.Parse(msgs[1]))
        {
            case RecvAlarm.LPanelInfo:
                LPanelInfo(msg);
                break;
            case RecvAlarm.UserEnter:
                UserEnterAlarm(msg);
                break;
            case RecvAlarm.DepositApply:

                break;
            case RecvAlarm.WithdrawApply:

                break;
        }
        return ret;
    }


    private static string RecvLoginModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(msg, "#!");
        switch ((RecvLogin)Int32.Parse(msgs[1]))
        {
            case RecvLogin.Login:
                AdminLogin(msg);
                break;

        }
        return ret;
    }

   

    private static string RecvPartnerModule(string msg)
    {
        string ret = "0";
        string index = msg.Substring(0, 10);
        string[] msgs = Regex.Split(msg, "#!");
        switch ((RecvPartner)Int32.Parse(msgs[1]))
        {
            case RecvPartner.GetMyRevenues:
                GetMyRevenues(msg);
                break;
        }
        return ret;
    }



    ///-------------------------------------------------------------------------------------------/// 

    private static void JacpotServer(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        //이미드가있어서 실패
        if ( msgs[2]=="2")
        {
            Dialog.RequestWarning("콜이 이미 들어가 있습니다.");
        }
    }

    private static void GetServer(string msg)
    {
        Tool.DeleteAllRows();
        string[] msgs = Regex.Split(msg, "#!");



        int server = Int32.Parse(msgs[2]);
        if (server != 0)
            Tool.ActiveRows(60);

        int lastCount = 3;
        //접속유저
        for (int i = 0; i < Int32.Parse(msgs[3]); i++)
        {
            int t = i * 14 + 3;
            if (server == 0)
            {
                Tool.AddRow(msgs[t + 1], msgs[t + 2], "유저", msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7], msgs[t + 8], msgs[t + 9], msgs[t + 10], msgs[t + 11], msgs[t + 12], msgs[t + 13], msgs[t + 14]);
            }
            else
            {
                Tool.SetRow(msgs[t + 2], "유저", msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7], msgs[t + 8], msgs[t + 9], msgs[t + 10], msgs[t + 11], msgs[t + 12], msgs[t + 13], msgs[t + 14]);
            }
            lastCount = t + 15;
        }
        //예시정보
        if (msgs[3] == "0")
            lastCount = 4;
        if (server != 0)
        {

            int exampleCount = Int32.Parse(msgs[lastCount]);
            for (int i = 0; i < exampleCount; i++)
            {
                Tool.SetExampleLevel(msgs[lastCount + 1], msgs[lastCount + 2]);
                lastCount += 2;
            }
        }
        //봇
        if (server != 0)
        {
            int botCount = Int32.Parse(msgs[++lastCount]);

            for (int i = 0; i < botCount; i++)
            {
                Tool.SetState(msgs[lastCount + 1], "봇");
                Tool.SetID(msgs[lastCount + 1], msgs[lastCount + 2]);
                lastCount += 2;
            }
        }

        if (server != 0)
        {
            int reserveCount = Int32.Parse(msgs[++lastCount]);

            for (int i = 0; i < reserveCount; i++)
            {
                Tool.SetState(msgs[lastCount + 1], "예약");
                Tool.SetID(msgs[lastCount + 1], msgs[lastCount + 2]);
                Tool.SetReserve(msgs[lastCount + 1], msgs[lastCount + 3]);
                lastCount += 3;
            }
        }
    }

    //1 11/ PW /3 Name /4 phoneNum /5 partner / grade / joinday / lastPlayDay /9 lastInputMoney /10 TotalInputMoney /11 lastOutMoney /12 TotalOutMoney /13 Memo
    private static void GetUserInfo(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        Transform dialog = Dialog.ActiveDialog("UserInfo");
        int index = 2;
        dialog.Find("InputPw").GetComponent<InputField>().text = msgs[index++];
        dialog.Find("Name").GetComponent<Text>().text = msgs[index++];
        dialog.Find("PhoneNumber").GetComponent<Text>().text = msgs[index++];
        dialog.Find("Partner").GetComponent<Text>().text = msgs[index++];
        dialog.Find("DropdownGrade").GetComponent<Dropdown>().value = Int32.Parse(msgs[index++]);
        dialog.Find("JoinDay").GetComponent<Text>().text = msgs[index++];
        dialog.Find("LastPlayDay").GetComponent<Text>().text = msgs[index++];
        dialog.Find("LastDeposit").GetComponent<Text>().text = msgs[index++];
        dialog.Find("TotalDeposit").GetComponent<Text>().text = msgs[index++];
        dialog.Find("LastWithdraw").GetComponent<Text>().text = msgs[index++];
        dialog.Find("TotalWithdraw").GetComponent<Text>().text = msgs[index++];
        dialog.Find("TotalRevenue").GetComponent<Text>().text = msgs[index++];
        dialog.Find("InputMemo").GetComponent<InputField>().text = msgs[index++];
    }


    private static void SetUserList(string msg)
    {
        UserInfo.DeleteAllRows();
        string[] msgs = Regex.Split(msg, "#!");

        int index = 1;

        int partnerCount = 0;
        int totalPageNum = 1;
        int nowPageNum = 1;
        int userCount = UserInfo.MAX;

        Int32.TryParse(msgs[2], out partnerCount);
        if(partnerCount != 0)
        {
            List<string> partners = new List<string>();
            partners.Add("All");
            for (int i = 0; i < partnerCount; i++)
            {
                partners.Add(msgs[3 + i]);
            }

            index += partnerCount;
            UserInfo.SetPartnerDropdown(partners);
        }

        


        Int32.TryParse(msgs[2+index], out totalPageNum);
        Int32.TryParse(msgs[3+index], out nowPageNum);
        Int32.TryParse(msgs[4+index], out userCount);

        UserInfo.SettingPage(nowPageNum, totalPageNum);
        for (int i = 0; i < userCount; i++)
        {
            int t = i * 15 + 4+index;
            UserInfo.AddUserInfoRow(msgs[t + 1], msgs[t + 2], msgs[t + 5], msgs[t + 7], msgs[t + 9], msgs[t + 13], msgs[t + 6], msgs[t + 10], msgs[t + 11], msgs[t + 12]);
        }
    }

    private static void GetChatBotCount(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        Dialog.SetChatBotCount(Int32.Parse(msgs[2]));
    }


    private static void GetJoinUserCount(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        UserInfo.SetNumOfUser(msgs[2], msgs[3]);

    }


    private static void GetReserveList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        Reserve.DeleteAllRows();

        for(int i = 0; i<count; i++)
        {
            int t = i * 5 + 2;
            Reserve.AddRow(i + "", msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4], msgs[t + 5]);
        }
    }

    
    private static void GetBanList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        Ban.DeleteAllRows();
        for(int i = 0; i< count; i++)
        {
            int t = i * 4 + 2;
            Ban.AddRow(i + "", msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4]);
        }
    }

    //10: GetRevenueList  / Count / date / deposit / bonus / service / withdraw/revenue / rate/--
    private static void GetRevenueList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        int last = 3;
        Revenue.DeleteAllRows();
        for (int i = 0; i < count; i++)
        {
            int t = i * 7 + 2;
            Revenue.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7]);
            last = t + 7;
        }
        Revenue.SetTotalRow(msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5], msgs[last + 6], msgs[last + 7]);
    }


    private static void GetPartnerListRate(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        List<string> partners = new List<string>();
        partners.Add("전체");
        for (int i =0; i<count; i++ )
        {
            partners.Add(msgs[3 + i]);
        }
        TermPartner.SetPartnerDropdown(partners);
    }

    // 12 : GetDepositList / Count /Num/ Date / ID / Name / partner / Money / Check/Process..
    private static void GetDepositList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        int last = 3;
        Deposit.DeleteAllRows();
        for (int i = 0; i < count; i++)
        {
            int t = i * 8 + 2;
            Deposit.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7], msgs[t+8]);
            last = t + 8;
        }
        if(count > 0)
            Deposit.SetTotalRow(Int32.Parse(msgs[last+1]));

    }

    private static void GetWithdrawList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        int last = 3;
        Withdraw.DeleteAllRows();
        for (int i = 0; i < count; i++)
        {
            int t = i * 10 + 2;
            Withdraw.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7], msgs[t + 8], msgs[t + 9], msgs[t + 10]);
            last = t + 10;
        }
        if(count > 0)
            Withdraw.SetTotalRow(Int32.Parse(msgs[last + 1]));

    }


    private static void DepositConfirmOK(string msg)
    {
        //음.. 알람 끄나 체크해야지!
        string[] msgs = Regex.Split(msg, "#!");
        int sound = Int32.Parse(msgs[3]);

        if (sound == 1)
        {
            Alarm.StopDepositAlarm();
        }
        else
        {
            Alarm.StartDepositAlarm();
        }

        Alarm.DepositNumber.text = msgs[2];
    }

    private static void WithdrawConfirmOK(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int sound = Int32.Parse(msgs[3]);

        if (sound == 1)
        {
            Alarm.StopWithdrawAlarm();
        }
        else
        {
            Alarm.StartWithdrawAlarm();
        }

        Alarm.WithdrawNumber.text = msgs[2];
    }

    private static void GetVirtualList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        Virtual.DeleteAllRows();
        for(int i =0; i<count; i++)
        {
            int t = i * 3 + 2;
            Virtual.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3]);
        }
    }

    private static void GetPartnerList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        Partner.DeleteAllRows();
        for (int i = 0; i < count; i++)
        {
            int t = i * 5 + 2;
            Partner.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4],msgs[t+5]);
        }
    }


    private static void GetCalculateList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        int last = 3;
        Calculate.DeleteAllRows();
        for (int i = 0; i < count; i++)
        {
            int t = i * 7 + 2;
            Calculate.AddRow(msgs[t + 1], msgs[t + 2], msgs[t + 3], msgs[t + 4], msgs[t + 5], msgs[t + 6], msgs[t + 7]);
            last = t + 7;
        }
        Calculate.SetTotalRow(msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5], msgs[last + 6], msgs[last + 7]);

    }


    private static void GetManagerInfo(string msg)
    {

        string[] msgs = Regex.Split(msg, "#!");
        Manager.SetManager(msgs[2], msgs[3], msgs[4], msgs[5], msgs[6], msgs[7], msgs[8], msgs[9], msgs[10],msgs[11],msgs[12]);
    }

    //한개받는거는 따로합시다.. 이건 문의버튼 눌렀을때만..
    private static void GetQuestionList(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        int count = Int32.Parse(msgs[2]);
        
        Question.DeleteAll();

        int lastIndex = 3;
        int qCount = 0;

        for (int i = 0; i<count; i++)
        {
            string id = msgs[lastIndex ];
            qCount = Int32.Parse(msgs[lastIndex + 1]);

            lastIndex += 2;
            for (int j = 0; j<qCount; j++)
            {
                Question.AddQChatItem(id, msgs[lastIndex], msgs[lastIndex + 1]);
                lastIndex += 2;

                if (j == qCount - 2)
                    Question.ForceScrollDown(id);
            }
            Question.ForceScrollDown(id);
        }
        
    }


    private static void GetBotNUserList(string msg)
    {
        Bot.DeleteAllRows();
        string[] msgs = Regex.Split(msg, "#!");
        int last = 0;
        for (int i = 0; i < Int32.Parse(msgs[2]); i++)
        {
            int t = i * 2 + 2;
            Bot.AddBotRow(i + "", msgs[t + 1], msgs[t + 2]);
            last = t + 3;
        }
        int count = Int32.Parse(msgs[last]);

        string[] des = new string[count];
        Array.Copy(msgs,last+1,des,0,count);
        List<string> list = des.ToList();
        Mecro.SetDropdown(list);
        Bot.SetDropdown(list);
        
    }
    //Type : (admin(공지), bot, user, 문의,응답)
    private static void GetChatLog(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");

        
        if (msgs[3] == "3")
        {
            Alarm.AlarmQuestion();
            msgs[3] = "문의";
            //문의 창 부분
            Question.AddQChatItem(msgs[2], msgs[2], msgs[4]);
            Question.ForceScrollDown(msgs[2]);
        }
        else if (msgs[3] == "4")
        {
            msgs[3] = "답변";
            Question.AddQChatItem(msgs[5], msgs[2], msgs[4]);
            Question.ForceScrollDown(msgs[5]);
        }
        else
        {
            if (!instance.ChatObject.activeSelf)
                Alarm.AlarmChat();
        }
        ChatLog.AddChatRow(msgs[3], msgs[2], msgs[4], msgs[5]);
        ChatLog.ForceScrollDown();
        
        

    }

    private static void GetChatLogList(string msg)
    {
        ChatLog.DeleteAllRow();

        string[] msgs = Regex.Split(msg, "#!");
        for (int i = 0; i < Int32.Parse(msgs[2]); i++)
        {

            int t = i * 4 + 2;
            if (msgs[t + 2] == "3")
                msgs[t + 2] = "문의";
            if (msgs[t + 2] == "4")
                msgs[t + 2] = "답변";
            ChatLog.AddChatRow(msgs[t + 2], msgs[t + 1], msgs[t + 3], msgs[t + 4]);
            if(Int32.Parse(msgs[2])-2 == i)
            {
                Canvas.ForceUpdateCanvases();
            }
        }
        ChatLog.ForceScrollDown();
    }

    private static void LPanelInfo(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        Alarm.Server100Number.text = msgs[2];
        Alarm.Server300Number.text = msgs[3];
        Alarm.Server500Number.text = msgs[4];
        Alarm.Server1000Number.text = msgs[5];
        Alarm.OnlineNumber.text = msgs[6];
        //Alarm.ChattingNumber.text = msgs[7];
        Alarm.UserNumber.text = msgs[8];
        Alarm.AdjustmentNumber.text = msgs[9];
        Alarm.PartnerNumber.text = msgs[10];
        Alarm.DepositNumber.text = msgs[11];
        Alarm.WithdrawNumber.text = msgs[12];
        //Alarm.QuestionNumber.text = msgs[13];
        if (msgs[14] == "0")
            Alarm.StartDepositAlarm();
        else
            Alarm.StopDepositAlarm();
        if (msgs[15] == "0")
            Alarm.StartWithdrawAlarm();
        else
            Alarm.StopDepositAlarm();
    }

    private static void UserEnterAlarm(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        switch (msgs[2])
        {
            case "0":
                Alarm.Add(Alarm.OnlineNumber, 1);
                break;
            case "1":
                Alarm.AddServer(msgs[3], -1);
                Alarm.AddServer(msgs[4], 1);
                break;
            case "2":
                Alarm.Add(Alarm.OnlineNumber, 1);
                Alarm.AddServer(msgs[4], 1);
                break;
            case "3":
                Alarm.AddServer(msgs[3], -1);
                Alarm.Add(Alarm.OnlineNumber, -1);
                break;
        }
        if(msgs[2]=="0" || msgs[2]=="2")
            Alarm.StartUserAlarm(msgs[5]);
    }

    private static void AdminLogin(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        //admin
        if (msgs[2]=="0")
        {
            instance.LoginObject.SetActive(false);
            instance.MainObject.SetActive(true);
            instance.LeftObject.SetActive(true);
            Alarm.IsMute = false;
        }
        //partner
        else if(msgs[2]=="1")
        {
            instance.LoginObject.SetActive(false);
            instance.PartnerPrivateObject.SetActive(true);
            //내용을 넣어야지.

            PrivatePage.PartnerID = msgs[3];

            int last = 5;
            int revenueCount = Int32.Parse(msgs[4]);
            for (int i = 0; i<revenueCount; i++)
            {
                PrivatePage.AddRow(msgs[last],msgs[last+1],msgs[last+2], msgs[last + 3], msgs[last + 4], msgs[last + 5], msgs[last + 6]);
                last += 7;
            }
            if(revenueCount != 0)
            { 
                PrivatePage.SetTotalRow(msgs[last], msgs[last + 1], msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5]);
                last += 6;
            }
            int userCount = Int32.Parse(msgs[last++]);
            for(int i = 0; i<userCount; i++)
            {
                PrivateUser.AddUserInfoRow(msgs[last], msgs[last + 1], msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5], msgs[last + 6], msgs[last + 7]);
                last += 8;
            }

        }
        //fail
        else if(msgs[2]=="2")
        {
            instance.LoginObject.transform.Find("LoginErrText").GetComponent<Text>().text = msgs[3];
        }
    }

    private static void GetMyRevenues(string msg)
    {
        string[] msgs = Regex.Split(msg, "#!");
        PrivatePage.DeleteAllRows();

        int last = 3;
        int revenueCount = Int32.Parse(msgs[2]);

        for (int i = 0; i < revenueCount; i++)
        {
            PrivatePage.AddRow(msgs[last], msgs[last + 1], msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5], msgs[last + 6]);
            last += 7;
        }
        if (revenueCount != 0)
        {
            PrivatePage.SetTotalRow(msgs[last], msgs[last + 1], msgs[last + 2], msgs[last + 3], msgs[last + 4], msgs[last + 5]);
            last += 6;
        }
        

    }

    // 패널이 추가됨에 따라 계속 수정해주어야함.
    private void NonActiveAllPanelsExcept(GameObject exception)
    {
        ToolObject.SetActive(false);
        ChatObject.SetActive(false);
        UserObject.SetActive(false);
        AdjustmentObject.SetActive(false);
        PartnerObject.SetActive(false);
        QuestionObject.SetActive(false);
        ManagerObject.SetActive(false);
        exception.SetActive(true);
    }

    private void NonActiveUserPanelExcept(GameObject exception)
    {
        UserInfosObject.SetActive(false);
        ReserveInfoObject.SetActive(false);
        BanInfoObject.SetActive(false);
        exception.SetActive(true);
    }

    private void NonActiveAdjustmentPanelExcept(GameObject exception)
    {
        RevenueListObject.SetActive(false);
        DepositListObject.SetActive(false);
        WithdrawListObject.SetActive(false);
        VirtualListObject.SetActive(false);
        exception.SetActive(true);
    }

    private void NonActivePartnerPanelExcept(GameObject exception)
    {
        PartnerListObject.SetActive(false);
        PartnerCalculateObject.SetActive(false);
        exception.SetActive(true);
    }

    #region ClickEvents

    public void Login()
    {
        string id = LoginObject.transform.Find("InputLoginID").GetComponent<InputField>().text;
        string pw = LoginObject.transform.Find("InputLoginPW").GetComponent<InputField>().text;
        Alarm.IsMute = true;
        SocketManager.Send("6#!1#!" + id + "#!" + pw);
    }

    public void LClickTool(int server)
    {
        NonActiveAllPanelsExcept(ToolObject);
        string sendMsg = "1#!10#!" + server;
        Tool.SetServer(server);
        Tool.InitSelectedIndex();
        SocketManager.Send(sendMsg);

        if (server == 0)
            NowPage = Page.All;
        else if (server == 100)
            NowPage = Page.Server100;
        else if (server == 300)
            NowPage = Page.Server300;
        else if (server == 500)
            NowPage = Page.Server500;
        else if (server == 1000)
            NowPage = Page.Server1000;
    }

    public void LClickChat()
    {
        NonActiveAllPanelsExcept(ChatObject);
        string sendMsg = "3#!1";
        SocketManager.Send(sendMsg);
        sendMsg = "3#!2";
        SocketManager.Send(sendMsg);
        SaveLoad.LoadMecros();
        NowPage = Page.None;
    }

    public void LClickAdminUser()
    {
        NonActiveAllPanelsExcept(UserObject);
        NonActiveUserPanelExcept(UserInfosObject);
        UserInfo.InitDropDown();
        string sendMsg = "2#!1#!All#!"+ UserInfo.MAX + "#!1";
        SocketManager.Send(sendMsg);

        SocketManager.Send("2#!4");
        NowPage = Page.None;

    }

    public void LClickAdjustment()
    {
        NonActiveAllPanelsExcept(AdjustmentObject);
        NonActiveAdjustmentPanelExcept(RevenueListObject);
        TermPartner.SetSelectedPanel(TermPanel.RevenueList);
        TermPartner.SetDateToday();
        TermPartner.InitPresets();
        Revenue.DeleteAllRows();

        SocketManager.Send("2#!11#!");
        NowPage = Page.None;

    }

    public void LClickPartner()
    {
        NonActiveAllPanelsExcept(PartnerObject);
        NonActiveAdjustmentPanelExcept(PartnerListObject);
        Partner.DeleteAllRows();
        SocketManager.Send("2#!18#!");
        NowPage = Page.None;

    }
    
    public void LClickQuestion()
    {
        NonActiveAllPanelsExcept(QuestionObject);
        List<string> list = Question.GetActiveQuestionIDList();
        string msg = "2#!25#!" + list.Count + "#!";
        for(int i = 0;i<list.Count; i++)
        {
            msg += list[i] + "#!";
        }
        SocketManager.Send(msg);
        Alarm.StopQuestionAlarm();
        Alarm.Set(Alarm.QuestionNumber, 0);
    }

    public void LClickManager()
    {
        NonActiveAllPanelsExcept(ManagerObject);
        SocketManager.Send("2#!21#!0");
        NowPage = Page.None;

    }

    public void TClickUserInfo()
    {
        NonActiveUserPanelExcept(UserInfosObject);
        string sendMsg = "2#!1#!"+UserInfo.GetSelectedPartner()+"#!"+UserInfo.MAX+"#!1";
        SocketManager.Send(sendMsg);
        SocketManager.Send("2#!4");
        NowPage = Page.None;

    }


    public void TClickReserveInfo()
    {
        NonActiveUserPanelExcept(ReserveInfoObject);
        string sendMsg = "2#!5";
        SocketManager.Send(sendMsg);
        NowPage = Page.None;

    }

    public void TClickBanInfo()
    {
        NonActiveUserPanelExcept(BanInfoObject);
        string sendMsg = "2#!6";
        SocketManager.Send(sendMsg);
        NowPage = Page.None;

    }

    public void TAdClickRevenue()
    {
        NonActiveAdjustmentPanelExcept(RevenueListObject);
        TermPartner.SetSelectedPanel(TermPanel.RevenueList);
        Revenue.DeleteAllRows();
        SocketManager.Send("2#!11#!");
        NowPage = Page.None;

    }

    public void TAdClickDeposit()
    {
        NonActiveAdjustmentPanelExcept(DepositListObject);
        TermPartner.SetSelectedPanel(TermPanel.DepositList);
        Deposit.DeleteAllRows();
        SocketManager.Send("2#!11#!");
        NowPage = Page.None;

    }

    public void TAdClickWithdraw()
    {
        NonActiveAdjustmentPanelExcept(WithdrawListObject);
        TermPartner.SetSelectedPanel(TermPanel.WithdrawList);
       // Revenue.DeleteAllRows();
        SocketManager.Send("2#!11#!");
        NowPage = Page.None;

    }

    public void TAdClickVirtual()
    {
        NonActiveAdjustmentPanelExcept(VirtualListObject);
        TermPartner.SetSelectedPanel(TermPanel.Virtual);
        SocketManager.Send("2#!17");
        NowPage = Page.None;

    }

    public void TPtClickManage()
    {
        NonActivePartnerPanelExcept(PartnerListObject);
        SocketManager.Send("2#!18");
        NowPage = Page.None;

    }

    public void TptClickCalculate()
    {
        NonActivePartnerPanelExcept(PartnerCalculateObject);
        NowPage = Page.None;

    }

    public void AlarmClickDeposit()
    {
        NonActiveAllPanelsExcept(AdjustmentObject);
        NonActiveAdjustmentPanelExcept(DepositListObject);
        TermPartner.SetSelectedPanel(TermPanel.DepositList);
        SocketManager.Send("2#!12#!"+DateTime.Now.ToShortDateString()+"#!"+DateTime.Now.ToShortDateString() + "#!0#!#!");
        

    }


    public void AlarmClickWithdraw()
    {
        NonActiveAllPanelsExcept(AdjustmentObject);
        NonActiveAdjustmentPanelExcept(WithdrawListObject);
        TermPartner.SetSelectedPanel(TermPanel.WithdrawList);

        SocketManager.Send("2#!13#!" + DateTime.Now.ToShortDateString() + "#!" + DateTime.Now.ToShortDateString() + "#!0#!#!");
    
    }

    public void AlarmClickQuestion()
    {
        LClickQuestion();
    }

    #endregion
    private void Refresh(object sender, ElapsedEventArgs e)
    {
        refreshTemp++;
        if(refreshTemp >= Manager.RefreshTime)
        {
            if(NowPage != Page.None)
            {
                SocketManager.Send("1#!10#!" + Tool.NowServer);
            }
            refreshTemp = 0;
        }
        
    }

    #region Timers


    #endregion

}

enum Page
{
    None, Server100, Server300, Server500, Server1000, All   
}

enum RecvType
{
    Tool = 1, Manage, Chat, Alarm = 5, Login, Partner
}

enum RecvTool
{
    JacpotServer = 1, GetServer = 10, GetUserInfo, GetChatBotCount=13
}


enum RecvManage
{
    GetUserList = 1 , GetJoinUser=4, GetReserveList, GetBanList,GetRevenueList=10,
    GetPartnerListRate =11, GetDepositList, GetWithdrawList, DepositConfirmOK, WithdrawConfirmOK,
    GetVirtualList = 17, GetPartnerList, GetCalculateList=20, GetManagerInfo, GetQuestionList=25

}

enum RecvChat
{
    GetBotNUserList = 1, GetChatLogList, GetChat
}

enum RecvAlarm
{
    LPanelInfo=1, UserEnter, DepositApply=4, WithdrawApply
}

enum RecvLogin
{
    Login = 1
}

enum RecvPartner
{
    GetMyRevenues = 1
}