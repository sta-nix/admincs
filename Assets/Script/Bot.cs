﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bot : MonoBehaviour {

    private static Bot instance;

    public GameObject BotTable; //RowGroup
    public Dropdown SendUser;
    public GameObject RowPrefab;

    public int selectListIndex;
    private List<GameObject> BotRowList = new List<GameObject>();

    void Awake()
    {
        instance = this;

    }
    

    public static void AddBotRow(string num, string id, string which)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.BotTable.transform);

            instance.BotRowList.Add(item);
            SetOnClick(instance.BotRowList.Count - 1);
        }
        else
        {
            item = instance.BotRowList[emptyRow];
            SetActiveBotRowList(emptyRow, true);
        }


        SetItem(item, "Num", "" + num);
        SetItem(item, "Id", id);
        SetItem(item, "Which", which);
        item.transform.Find("Content").GetComponent<InputField>().text = "";
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.BotRowList.Count; i++)
        {
            SetActiveBotRowList(i, false);
        }
    }

    public static string GetSelectedUserID()
    {
        return instance.BotRowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text;
    }

    public static string GetSelectedChat()
    {
        return instance.BotRowList[instance.selectListIndex].transform.Find("Content").GetComponent<InputField>().text;
    }
    
    public static void SetDropdown(List<string> list)
    {
        instance.SendUser.options.Clear();
        instance.SendUser.options.Add(new Dropdown.OptionData("전체전송"));
        instance.SendUser.AddOptions(list);
    }

    /*******************************************************************************************************/

    private static void SetActiveBotRowList(int index, bool state)
    {
        if (instance.BotRowList.Count > index)
        {
            instance.BotRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.BotRowList.Count; i++)
        {
            if (!instance.BotRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnClick(int index)
    {
        instance.BotRowList[index].GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; SendBotChat(index);  });


        instance.selectListIndex = index;
        InputField inputField = instance.BotRowList[index].transform.Find("Content").GetComponent<InputField>();
        inputField.onEndEdit.AddListener(val =>
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                instance.selectListIndex = index;
                SendBotChat(index);
                inputField.Select();
                inputField.ActivateInputField();
            }
        });
    }

    private static void SendBotChat(int index)
    {
        string user = "";
        if (instance.SendUser.value == 0)
            user = "0";
        else
        {
            user = instance.SendUser.options[instance.SendUser.value].text;
        }

        string sendContent = Set1000Char(GetSelectedChat());

        SocketManager.Send("3#!3#!"+GetSelectedUserID()+"#!"+sendContent+"#!"+user);

        instance.BotRowList[index].transform.Find("Content").GetComponent<InputField>().text = "";
        Logs.AddLog("[Chat]" + instance.SendUser.options[instance.SendUser.value].text + "에게 Bot 메시지 전송");

    }

    private static string Set1000Char(string str)
    {
        if(str.Length > 1000)
        {
            return str.Substring(0, 1000);
        }
        return str;
    }

   
}
