﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Revenue : MonoBehaviour {

    private static Revenue instance;

    public GameObject RevenueTable; //RowGroup
    public GameObject RowPrefab;
    public GameObject TotalRow;

    public int selectListIndex;
    private List<GameObject> RowList = new List<GameObject>();

    void Awake()
    {
        instance = this;

    }


    public static void AddRow(string date, string deposit, string bonus, string service, string withdraw, string revenue, string rate)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.RevenueTable.transform);

            instance.RowList.Add(item);
        }
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }


        SetItem(item, "Date", "" + date);
        SetItem(item, "DepositMoney", "" + deposit);
        SetItem(item, "Bonus", "" + bonus);
        SetItem(item, "Service", "" + service);
        SetItem(item, "WithdrawMoney", "" + withdraw);
        SetItem(item, "Revenue", "" + revenue);
        SetItem(item, "Rate", "" + rate);

    }

    public static void SetTotalRow(string deposit, string bonus, string service, string withdraw, string revenue, string rate)
    {
        SetItem(instance.TotalRow, "DepositMoney", "" + deposit);
        SetItem(instance.TotalRow, "Bonus", "" + bonus);
        SetItem(instance.TotalRow, "Service", "" + service);
        SetItem(instance.TotalRow, "WithdrawMoney", "" + withdraw);
        SetItem(instance.TotalRow, "Revenue", "" + revenue);
        SetItem(instance.TotalRow, "Rate", "" + rate);
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
        ClearTotalRow();
    }

    public static string GetSelectedUserID()
    {
        return instance.RowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text;
    }

    /*******************************************************************************************************/

    private static void ClearTotalRow()
    {
        SetTotalRow("", "", "", "", "", "");
    }

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.RowList.Count > index)
        {
            instance.RowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            if (!instance.RowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnClick(int index)
    {
        instance.RowList[index].GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; SendBanCancel(); });
    }

    private static void SendBanCancel()
    {
        SocketManager.Send("2#!7#!" + GetSelectedUserID());
    }
}
