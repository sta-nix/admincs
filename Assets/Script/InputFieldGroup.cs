﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputFieldGroup : MonoBehaviour
{
    List<InputField> fields = new List<InputField>();
    void Update()
    {

        //자신의 개체가 선택되었는지 확인.
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            SetInputFieldGroup();
            if (Input.GetKey(KeyCode.LeftShift))
            {

                for (int i = 0; i < fields.Count; i++)
                {
                    if (fields[i].isFocused)
                    {
                        if (i - 1 == -1)
                            fields[fields.Count - 1].Select();
                        else
                            fields[i - 1].Select();
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    for (int i = 0; i < fields.Count; i++)
                    {
                        if (fields[i].isFocused)
                        {
                            if (i + 1 == fields.Count)
                            {
                                fields[0].Select();
                                
                            }
                            else
                                fields[i + 1].Select();
                        }
                    }
                }
            }
        }
    }


    private void SetInputFieldGroup()
    {
        List<Transform> list = new List<Transform>(transform.GetComponentsInChildren<Transform>()) ;
        fields.Clear();

        for(int i = 0; i<list.Count; i++)
        {
            if(list[i].GetComponent<InputField>()!=null)
            fields.Add(list[i].GetComponent<InputField>());
        }
    }

    
}
