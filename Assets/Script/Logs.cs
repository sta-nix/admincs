﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Logs :MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static Logs instance;

    public Transform logList;
    public Transform extention;
    public Text oneLog;


    void Awake()
    {
        instance = this;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        UpTotalLog();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        DownTotalLog();
    }
    

    void OnMouseDown()
    {
        DownTotalLog();
    }

    public static void AddLog(string log)
    {
        string txt = "[" + DateTime.Now.ToShortTimeString() + "] "+log;
        OneLog.text = txt;
        Text logObject = Instantiate(OneLog, LogList);
        
    }

    public void UpTotalLog()
    {
        Vector3 exVec = Extention.position;
        exVec.y = 0;
        Extention.position = exVec;
    }

    public void DownTotalLog()
    {
        Vector3 exVec = Extention.position;
        exVec.y = -200;
        Extention.position = exVec;
    }

    #region Setter Getter

    public static Transform LogList
    {
        get
        {
            return instance.logList;
        }

        set
        {
            instance.logList = value;
        }
    }

    public static Transform Extention
    {
        get
        {
            return instance.extention;
        }

        set
        {
            instance.extention = value;
        }
    }

    public static Text OneLog
    {
        get
        {
            return instance.oneLog;
        }

        set
        {
            instance.oneLog = value;
        }
    }
    #endregion
}
