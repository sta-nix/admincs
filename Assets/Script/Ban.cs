﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ban : MonoBehaviour {

    private static Ban instance;

    public GameObject BanTable; //RowGroup
    public GameObject RowPrefab;

    public int selectListIndex;
    private List<GameObject> BanRowList = new List<GameObject>();

    void Awake()
    {
        instance = this;

    }


    public static void AddRow(string num, string id, string name,string phone, string reason)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.BanTable.transform);

            instance.BanRowList.Add(item);
            SetOnModifiedClick(instance.BanRowList.Count - 1);
            SetOnIDClick(instance.BanRowList.Count - 1);
        }
        else
        {
            item = instance.BanRowList[emptyRow];
            SetActiveBotRowList(emptyRow, true);
        }


        SetItem(item, "Num", "" + num);
        SetItem(item, "Id", id);
        SetItem(item, "Name", name);
        SetItem(item, "PhoneNumber", phone);
        SetItem(item, "Reason", reason);
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.BanRowList.Count; i++)
        {
            SetActiveBotRowList(i, false);
        }
    }

    public static string GetSelectedUserID()
    {
        return instance.BanRowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text;
    }
    
    /*******************************************************************************************************/

    private static void SetActiveBotRowList(int index, bool state)
    {
        if (instance.BanRowList.Count > index)
        {
            instance.BanRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.BanRowList.Count; i++)
        {
            if (!instance.BanRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnModifiedClick(int index)
    {
        instance.BanRowList[index].transform.Find("Modified").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; SendBanCancel(); });
    }

    private static void SetOnIDClick(int index)
    {
        instance.BanRowList[index].transform.Find("Id").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; ShowUserInfo();});

    }

    private static void SendBanCancel()
    {
        SocketManager.Send("2#!7#!" + GetSelectedUserID());
    }

    private static void ShowUserInfo()
    {
        string id = GetSelectedUserID();
        if (id == " ")
            return;
        Dialog.RequestUserInfo(id);
    }
}
