﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;


//Table 객체의 검사로 구현하려면 Header가 0번에 들어있다는 것을 고려해야한다.
//재생성할 필요가 없지.

public class Tool : MonoBehaviour
{

    public static Tool instance;
    public GameObject Table;
    public GameObject RowPrefab;
    public int selectListIndex = -1;
    public List<GameObject> RowList = new List<GameObject>();
    private int Server;

 

    // Use this for initialization
    void Awake()
    {
        instance = this;

    }

    void Start()
    {
        for (int i = 0; i < 60; i++)
            AddRowEmpty("" + i);
    }

    public static string Clipboard
    {
        get { return GUIUtility.systemCopyBuffer; }
        set { GUIUtility.systemCopyBuffer = value; }
    }

    public void AddRowTest()
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
            item = Instantiate(instance.RowPrefab, instance.Table.transform);
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }

        instance.RowList.Add(item);
        SetOnClick(instance.RowList.Count - 1);
        SetItem(item, "Num", "으앙킼");
    }

    public static void InitSelectedIndex()
    {
        instance.selectListIndex = -1;
    }

    public static void SetServer(int s)
    {
        instance.Server = s;
    }

    public static int GetSelectedIndex()
    {
        
        return instance.selectListIndex;
    }

    public static string GetID()
    {
        return GetItem(instance.RowList[instance.selectListIndex], "Id");
    }

    public static bool IsBot()
    {
        if(GetItem(instance.RowList[instance.selectListIndex],"State") == "봇")
            return true;
        return false;
    }

    public static string GetID(int index)
    {
        if (index < 0)
            return " ";
        return GetItem(instance.RowList[index], "Id");
    }

    public static string GetExample()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "ExLevel");
    }

    public static string GetMoney()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "Money");
    }

    public static string GetSpin()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "Spin");
    }

    public static string GetHoldState()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "Hold");
    }

    public static string GetGiftExchange()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "GiftEx");
    }

    public static string GetMoneyExchange()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "MoneyEx");
    }

    public static string GetReserve()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.RowList[instance.selectListIndex], "Reserve");
    }
    

    public static string GetServer(int index)
    {
        if (index < 0)
            return " ";
        if (instance.Server == 0)
        {
            string num = GetItem(instance.RowList[index], "Num");
            string[] server = Regex.Split(num, "/");
            return server[0];
        }
        else
            return ""+instance.Server;
    }

   

    public static string GetMachine()
    {
        if (instance.selectListIndex < 0)
            return " ";
        if (instance.Server == 0)
        {
            string num = GetItem(instance.RowList[instance.selectListIndex], "Num");
            string[] server = Regex.Split(num, "/");
            return server[1];
        }
        else
            return "" + instance.selectListIndex;
    }

    public static string GetServer()
    {
        if (instance.selectListIndex < 0)
            return " ";
        if (instance.Server == 0)
        {
            string num = GetItem(instance.RowList[instance.selectListIndex], "Num");
            string[] server = Regex.Split(num, "/");
            return server[0];
        }
        else
            return "" + instance.Server;
    }

    public static void ActiveRows(int Count)
    {
        for (int i = 0; i < Count; i++)
        {
            instance.RowList[i].SetActive(true);
            SetRow(""+i, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "," ");

        }
    }

    public static void DeleteRowTest()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
    }
    

    public static void SetRow(string number,string state, string id, string money, string gift, string spin, string hold, string giftEx, string moneyEx, string exLevel, string reserve, string memo, string jacpot, string wait)
    {
        GameObject item  = instance.RowList[Int32.Parse(number)];
        SetItem(item, "Num", number);
        SetItem(item, "State", state);
        SetItem(item, "Id", id);
        SetItem(item, "Money", money);
        SetItem(item, "Gift", gift);
        SetItem(item, "Spin", spin);
        hold = hold == "0" ?  "X" : "O";
        if (id == " ")hold = " ";
        SetItem(item, "Hold", hold);
        giftEx = giftEx == "0" ? "가능" : "불가";
        if (id == " ") giftEx = " ";
        SetItem(item, "GiftEx", giftEx);
        moneyEx = moneyEx == "0" ? "가능" : "불가";
        if (id == " ") moneyEx = " ";
        SetItem(item, "MoneyEx", moneyEx);
        SetItem(item, "ExLevel", exLevel);
        SetItem(item, "Reserve", reserve);
        SetItem(item, "Memo", memo);
        SetItem(item, "Jacpot", jacpot);
        SetItem(item, "Wait", wait);
    }

    public static void AddRowEmpty(string num)
    {
        AddRow(" ", num," ", " ", " ", " ", " ", " ", " ", " ", " ", " "," ", " ", " ");
    }

    public static void SetExampleLevel(string number, string ex)
    {
        GameObject item = instance.RowList[Int32.Parse(number)];

        SetItem(item, "ExLevel", ex);
    }

    public static void SetState(string number, string state)
    {
        GameObject item = instance.RowList[Int32.Parse(number)];

        SetItem(item, "State", state);
    }

    public static void SetID(string number, string id)
    {
        GameObject item = instance.RowList[Int32.Parse(number)];

        SetItem(item, "Id", id);
    }

    public static void SetReserve(string number, string time)
    {
        GameObject item = instance.RowList[Int32.Parse(number)];

        SetItem(item, "Reserve", time);
    }

    public static void AddRow(string server, string number, string state, string id, string money, string gift, string spin,string hold, string giftEx, string moneyEx, string exLevel, string reserve, string memo, string jacpot, string wait)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.Table.transform);
            instance.RowList.Add(item);
            SetOnClick(instance.RowList.Count - 1);
        }
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }

        if(server == " ")
            SetItem(item, "Num", number);
        else
        {
            if (server == "-1")
                SetItem(item, "Num", "대기실");
            else
                SetItem(item, "Num", server + "/" + number);
        }

        SetItem(item, "State", state);
        SetItem(item, "Id", id);
        SetItem(item, "Money", money);
        SetItem(item, "Gift", gift);
        SetItem(item, "Spin", spin);
        hold = hold == "0" ? "X" : "O";
        if (id == " ") hold = " ";
        SetItem(item, "Hold", hold);
        giftEx = giftEx == "0" ? "가능" : "불가";
        if (id == " ") giftEx = " ";
        SetItem(item, "GiftEx", giftEx);
        moneyEx = moneyEx == "0" ? "가능" : "불가";
        if (id == " ") moneyEx = " ";
        SetItem(item, "MoneyEx", moneyEx);
        SetItem(item, "ExLevel", exLevel);
        SetItem(item, "Reserve", reserve);
        SetItem(item, "Memo", memo);
        SetItem(item, "Jacpot", jacpot);
        SetItem(item, "Wait", wait);
    }

    public static void DeleteAllRows()
    {
        for(int i = 0; i< instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
    }

    /*******************************************************************************************************/

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.RowList.Count > index)
        {
            instance.RowList[index].SetActive(state);
        }
    }
    
    private static int GetUnusedRowIndex()
    {
        for(int i = 0; i< instance.RowList.Count; i++)
        {
            if (!instance.RowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static string GetItem(GameObject row, string name)
    {
        return row.transform.Find(name).GetComponent<Text>().text;
    }

    private static void SetOnClick(int index)
    {
        instance.RowList[index].GetComponent<Button>().onClick.AddListener(() => {
            instance.selectListIndex = index;
            Clipboard = GetID();
        });
    }


    public static int NowServer
    {
        get
        {
            return instance.Server;
        }

        set
        {
            instance.Server = value;
        }
    }
}

