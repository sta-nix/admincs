﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class Reserve : MonoBehaviour {
    private static Reserve instance;

    public GameObject ReserveTable; //RowGroup
    public GameObject RowPrefab;

    public int selectListIndex;
    private List<GameObject> ReserveRowList = new List<GameObject>();

    void Awake()
    {
        instance = this;

    }


    public static void AddRow(string num, string which, string id, string example, string remain, string memo)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.ReserveTable.transform);

            instance.ReserveRowList.Add(item);
            SetModifiedOnClick(instance.ReserveRowList.Count - 1);
            SetIDOnClick(instance.ReserveRowList.Count - 1);
        }
        else
        {
            item = instance.ReserveRowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }


        SetItem(item, "Num", "" + num);
        SetItem(item, "Which", which);
        SetItem(item, "Id", id);
        SetItem(item, "Example", example);
        SetItem(item, "RemainTime", remain);
        SetItem(item, "Memo", memo);
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.ReserveRowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
    }

    public static int GetSelectedIndex()
    {
        return instance.selectListIndex;
    }

    public static string GetSelectedUserID()
    {
        return instance.ReserveRowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text;
    }

    public static string GetMachine()
    {
        if (instance.selectListIndex < 0)
            return " ";
        
        string num = GetItem(instance.ReserveRowList[instance.selectListIndex], "Which");
        string[] server = Regex.Split(num, "/");
        return server[1];
        
    }

    public static string GetServer()
    {
        if (instance.selectListIndex < 0)
            return " ";

        string num = GetItem(instance.ReserveRowList[instance.selectListIndex], "Which");
        string[] server = Regex.Split(num, "/");
        return server[0];
    }

    public static string GetReserve()
    {
        if (instance.selectListIndex < 0)
            return " ";
        return GetItem(instance.ReserveRowList[instance.selectListIndex], "RemainTime");
    }

    /*******************************************************************************************************/

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.ReserveRowList.Count > index)
        {
            instance.ReserveRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.ReserveRowList.Count; i++)
        {
            if (!instance.ReserveRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetModifiedOnClick(int index)
    {
        instance.ReserveRowList[index].transform.Find("Modified").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; Dialog.RequestReserveForTable(); });
    }
    private static void SetIDOnClick(int index)
    {
        instance.ReserveRowList[index].transform.Find("Id").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; ShowUserInfo();  });

    }

    private static string GetItem(GameObject row, string name)
    {
        return row.transform.Find(name).GetComponent<Text>().text;
    }

    private static void ShowUserInfo()
    {
        string id = GetSelectedUserID();
        if (id == " ")
            return;
        Dialog.RequestUserInfo(id);
    }


}
