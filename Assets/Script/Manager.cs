﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

    public static Manager instance;


    public InputField adminID;
    public InputField adminPW;
    public InputField joinBonus;
    public InputField realBonus;
    public InputField vIPBonus;
    public InputField spinNum;
    public InputField gameRestTime;
    public InputField adminCode;
    public InputField autoReserveTime;
    public InputField refreshTerm;
    public Text userChatState;

    private int refreshTime=2;

    public static void SetManager(string adminID, string adminPW, string joinBonus, string realBonus, string vIPBonus, string spinNum, string code, string gameRestTime, string autoReserveTime, string refreshterm, string userChat="0")
    {
        AdminID.text = adminID;
        AdminPW.text = adminPW;
        VIPBonus.text = vIPBonus;
        SpinNum.text = spinNum;
        GameRestTime.text = gameRestTime;
        AutoReserveTime.text = autoReserveTime;
        AdminCode.text = code;
        JoinBonus.text = joinBonus;
        RealBonus.text = realBonus;
        RefreshTerm.text = refreshterm;
        RefreshTime = Int32.Parse(refreshterm);
        if (userChat == "0")
            UserChatState.text = "유저채팅(비허용중)";
        else
            UserChatState.text = "유저채팅(허용중)";
    }

    public void ClickAdminID()
    {
        SocketManager.Send("2#!22#!"+(int)ModifyManager.id+"#!"+AdminID.text);
    }

    public void ClickAdminPW()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.pw + "#!" + AdminPW.text);

    }

    public void ClickJoinBonus()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.joinB + "#!" + JoinBonus.text);

    }

    public void ClickRealBonus()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.realB + "#!" + RealBonus.text);

    }

    public void ClickVIPBonus()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.vipB+ "#!" + VIPBonus.text);

    }

    public void ClickSpinNum()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.spin + "#!" + SpinNum.text);

    }

    public void ClickAdminCode()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.code + "#!" + AdminCode.text);

    }

    public void ClickGameRestTime()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.gameRest + "#!" + GameRestTime.text);

    }

    public void ClickAutoReserveTime()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.autoReserve + "#!" + AutoReserveTime.text);

    }

    public void ClickRefreshTerm()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.refresh + "#!" + RefreshTerm.text);

    }

    public void ClickAllClientLogout()
    {
        Dialog.RequestConfirmDialog(Confirm.ClientAllLogout);
    }

    public void ClickUserChatOn()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.userChat + "#!1");
        UserChatState.text = "유저채팅(허용중)";
    }

    public void ClickUserChatOff()
    {
        SocketManager.Send("2#!22#!" + (int)ModifyManager.userChat + "#!0");
        UserChatState.text = "유저채팅(비허용중)";
    }

    void Awake()
    {
        instance = this;
    }
    
    public static InputField AdminID
    {
        get
        {
            return instance.adminID;
        }

        set
        {
            instance.adminID = value;
        }
    }

    public static InputField AdminPW
    {
        get
        {
            return instance.adminPW;
        }

        set
        {
            instance.adminPW = value;
        }
    }

    public static InputField JoinBonus
    {
        get
        {
            return instance.joinBonus;
        }

        set
        {
            instance.joinBonus = value;
        }
    }

    public static InputField RealBonus
    {
        get
        {
            return instance.realBonus;
        }

        set
        {
            instance.realBonus = value;
        }
    }

    public static InputField VIPBonus
    {
        get
        {
            return instance.vIPBonus;
        }

        set
        {
            instance.vIPBonus = value;
        }
    }

    public static InputField SpinNum
    {
        get
        {
            return instance.spinNum;
        }

        set
        {
            instance.spinNum = value;
        }
    }

    public static InputField GameRestTime
    {
        get
        {
            return instance.gameRestTime;
        }

        set
        {
            instance.gameRestTime = value;
        }
    }

    public static InputField AutoReserveTime
    {
        get
        {
            return instance.autoReserveTime;
        }

        set
        {
            instance.autoReserveTime = value;
        }
    }

    public static InputField AdminCode
    {
        get
        {
            return instance.adminCode;
        }

        set
        {
            instance.adminCode = value;
        }
    }

    public static InputField RefreshTerm
    {
        get
        {
            return instance.refreshTerm;
        }

        set
        {
            instance.refreshTerm = value;
        }
    }

    public static int RefreshTime
    {
        get
        {
            return instance.refreshTime;
        }

        set
        {
            instance.refreshTime = value;
        }
    }

    public static Text UserChatState
    {
        get
        {
            return instance.userChatState;
        }

        set
        {
            instance.userChatState = value;
        }
    }
}

enum ModifyManager
{
    id, pw, joinB, realB, vipB, spin, code, gameRest, autoReserve,refresh,userChat
}
