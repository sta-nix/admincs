﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Virtual : MonoBehaviour {

    private static Virtual instance;


    public GameObject Table; //RowGroup
    public GameObject RowPrefab;
    public InputField IF_ID, IF_Money;

    private List<GameObject> RowList = new List<GameObject>();

    public int selectListIndex;

    void Awake()
    {
        instance = this;
    }

    public static void AddRow( string id, string money, string date)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.Table.transform);

            instance.RowList.Add(item);

        }
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }

        
        SetItem(item, "Date", "" + date);
        SetItem(item, "Id", "" + id);
        SetItem(item, "Money", "" + money);

    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
    }

    /*******************************************************************************************************/

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.RowList.Count > index)
        {
            instance.RowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            if (!instance.RowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }
    
    public void ClickAddVirtual()
    {
        string date = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.ToShortTimeString();
        SocketManager.Send("2#!16#!" + IF_ID.text + "#!" + IF_Money.text);
        AddRow(IF_ID.text, IF_Money.text,date);
    }

}
