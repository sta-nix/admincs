﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivateUser : MonoBehaviour {

    private static PrivateUser instance;

    public GameObject UserInfoTable; //RowGroup
    public GameObject RowPrefab;
    

    public int selectListIndex;
    private List<GameObject> UserInfoRowList = new List<GameObject>();

    void Awake()
    {
        instance = this;
    }
	
    
    public static void AddUserInfoRow( string id, string name, string money, string gift, string grade, string partner, string joinDay, string lastPlayDate)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.UserInfoTable.transform);

            instance.UserInfoRowList.Add(item);

        }

        else
        {
            item = instance.UserInfoRowList[emptyRow];
            SetActiveUserInfoRowList(emptyRow, true);
        }

        
        SetItem(item, "Id", id);
        SetItem(item, "Name", name);
        SetItem(item, "Money", money);
        SetItem(item, "Gift", gift);

        SetItem(item, "Grade", grade);
        SetItem(item, "Partner", partner);
        SetItem(item, "JoinDay", joinDay);
        SetItem(item, "LastPlayDay", lastPlayDate);

    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.UserInfoRowList.Count; i++)
        {
            SetActiveUserInfoRowList(i, false);
        }
    }

    public static string GetSelectedUserID()
    {
        return instance.UserInfoRowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text; ;
    }

   
    /*******************************************************************************************************/

    private static void SetActiveUserInfoRowList(int index, bool state)
    {
        if (instance.UserInfoRowList.Count > index)
        {
            instance.UserInfoRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.UserInfoRowList.Count; i++)
        {
            if (!instance.UserInfoRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void ShowUserInfo()
    {
        string id = GetSelectedUserID();
        if (id == " ")
            return;
        Dialog.RequestUserInfo(id);
    }

}
