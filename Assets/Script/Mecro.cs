﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Mecro : MonoBehaviour {


    private static Mecro instance;

    public GameObject MecroTable; //RowGroup
    public Dropdown SendUser;

    public GameObject RowPrefab;

    public int selectListIndex;
    private List<GameObject> MecroRowList = new List<GameObject>();

    void Awake()
    {
        instance = this;

    }

    void Start()
    {
        for(int i = 1; i < 21; i++)
        {
            AddMecroRow(i+"");
        }
        AddMecroRow("일반");
    }

    public static void AddMecroRow(string num)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.MecroTable.transform);

            instance.MecroRowList.Add(item);
            SetOnClick(instance.MecroRowList.Count - 1);
            SetOnValueChanged(instance.MecroRowList.Count - 1);
        }
        else
        {
            item = instance.MecroRowList[emptyRow];
            SetActiveMecroRowList(emptyRow, true);
        }


        SetItem(item, "Num", "" + num);
        item.transform.Find("Content").GetComponent<InputField>().text = "";
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.MecroRowList.Count; i++)
        {
            SetActiveMecroRowList(i, false);
        }
    }
    

    public static string GetSelectedChat()
    {
        return instance.MecroRowList[instance.selectListIndex].transform.Find("Content").GetComponent<InputField>().text;
    }

    public static string GetMecro(int index)
    {
        if(index > -1 && index < 20)
            return instance.MecroRowList[index].transform.Find("Content").GetComponent<InputField>().text;
        return "";
    }

    public static void SetMecro(int index, string value)
    {
        if (index > -1 && index < 20)
            instance.MecroRowList[index].transform.Find("Content").GetComponent<InputField>().text = value;
            
    }

    public static void SetDropdown(List<string> list)
    {
        instance.SendUser.options.Clear();
        instance.SendUser.options.Add(new Dropdown.OptionData("전체전송"));
        instance.SendUser.AddOptions(list);
    }

    /*******************************************************************************************************/

    private static void SetActiveMecroRowList(int index, bool state)
    {
        if (instance.MecroRowList.Count > index)
        {
            instance.MecroRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.MecroRowList.Count; i++)
        {
            if (!instance.MecroRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnValueChanged(int index)
    {
        //instance.MecroRowList[index].GetComponentInChildren<InputField>().onValueChanged.AddListener((val) => {
        //    SaveLoad.SaveMecros();
        //});
        instance.MecroRowList[index].GetComponentInChildren<InputField>().onEndEdit.AddListener((val) => {
            SaveLoad.SaveMecros();
        });


    }

    private static void SetOnClick(int index)
    {
        instance.MecroRowList[index].GetComponentInChildren<Button>().onClick.AddListener(() => {
            instance.selectListIndex = index;
            SendMecroChat();

        });

        if (index == 20)
        {
            instance.selectListIndex = index;
            InputField inputField = instance.MecroRowList[index].transform.Find("Content").GetComponent<InputField>();
            inputField.onEndEdit.AddListener(val =>
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                {
                    instance.selectListIndex = 20;
                    SendMecroChat();
                    inputField.Select();
                    inputField.ActivateInputField();
                }
            });
        }

    }

    private static void SendMecroChat()
    {
        string user = "";
        if (instance.SendUser.value == 0)
            user = "0";
        else
        {
            user = instance.SendUser.options[instance.SendUser.value].text;
        }
        string sendContent = Set1000Char(GetSelectedChat());
        SocketManager.Send("3#!4#!"+ sendContent +"#!"+user);
        if(instance.selectListIndex == 20)
        {
            instance.MecroRowList[20].transform.Find("Content").GetComponent<InputField>().text = "";

        }
        Logs.AddLog("[Chat]"+ instance.SendUser.options[instance.SendUser.value].text + "에게 Mecro 전송");
    }

    private static string Set1000Char(string str)
    {
        if (str.Length > 1000)
        {
            return str.Substring(0, 1000);
        }
        return str;
    }
}
