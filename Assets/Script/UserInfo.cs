﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInfo : MonoBehaviour {

    private static UserInfo instance;

    public GameObject UserInfoTable; //RowGroup
    public GameObject RowPrefab;

    public Transform PageBtnParent;

    public Dropdown PartnerDropdown;


    public Text NumOfUser;
    [SerializeField] private int max;


    public int selectListIndex;
    private List<GameObject> UserInfoRowList = new List<GameObject>();
    private List<GameObject> PageNumberList = new List<GameObject>();

    private int nowPage=1;

    void Awake()
    {
        instance = this;

    }

    private void Start()
    {
        for (int i = 0; i < 12; i++)
        {
            instance.PageNumberList.Add(instance.PageBtnParent.GetChild(i).gameObject);
            SetOnClickPageButton(i);
        }
        PartnerDropdown.onValueChanged.AddListener(delegate { DropdownValueChanged(PartnerDropdown); } );
    }

    private void DropdownValueChanged(Dropdown change)
    {
        SocketManager.Send("2#!1#!" + GetSelectedPartner() + "#!" + MAX + "#!" + 1);
    }


    public static void AddUserInfoRow(string num,  string id, string name, string money, string gift, string grade, string partner, string phoneNum, string joinDay, string lastPlayDate)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.UserInfoTable.transform);

            instance.UserInfoRowList.Add(item);
            SetOnClickDelete(instance.UserInfoRowList.Count - 1);
            SetOnClickInfo(instance.UserInfoRowList.Count - 1);

        }

        else
        {
            item = instance.UserInfoRowList[emptyRow];
            SetActiveUserInfoRowList(emptyRow, true);
        }


        SetItem(item, "Num", ""+num);
        SetItem(item, "Id", id);
        SetItem(item, "Name", name);
        SetItem(item, "Money", money);
        SetItem(item, "Gift", gift);

        SetItem(item, "Grade", grade);
        SetItem(item, "Partner", partner);
        SetItem(item, "PhoneNumber", phoneNum);
        SetItem(item, "JoinDay", joinDay);
        SetItem(item, "LastPlayDay", lastPlayDate);

    }

    
    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.UserInfoRowList.Count; i++)
        {
            SetActiveUserInfoRowList(i, false);
        }
    }

    public static string GetSelectedUserID()
    {
        return instance.UserInfoRowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text; ;
    }

    public static void SetNumOfUser(string  total, string today)
    {
        instance.NumOfUser.text = "전체 " + total + "명 / 금일가입 " + today + "명";
    }

    public static void SetPartnerDropdown(List<string> ids)
    {
        instance.PartnerDropdown.ClearOptions();

        instance.PartnerDropdown.AddOptions(ids);
    }

    public static string GetSelectedPartner()
    {
        return instance.PartnerDropdown.options[instance.PartnerDropdown.value].text;
    }

    public static void InitDropDown()
    {
        instance.PartnerDropdown.value = 0;
    }

    /*******************************************************************************************************/

    private static void SetActiveUserInfoRowList(int index, bool state)
    {
        if (instance.UserInfoRowList.Count > index)
        {
            instance.UserInfoRowList[index].SetActive(state);
        }
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.UserInfoRowList.Count; i++)
        {
            if (!instance.UserInfoRowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetOnClickDelete(int index)
    {
        instance.UserInfoRowList[index].transform.Find("Delete").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; DeleteUser(); });
    }

    private static void SetOnClickInfo(int index)
    {
        instance.UserInfoRowList[index].transform.Find("Id").GetComponentInChildren<Button>().onClick.AddListener(() => { instance.selectListIndex = index; ShowUserInfo(); });
    }

    private void SetOnClickPageButton(int i)
    {
        instance.PageNumberList[i].GetComponent<Button>().onClick.AddListener(() => { ClickPageButton(i); });
    }


    private static void DeleteUser()
    {
        Dialog.RequestConfirmDialog(Confirm.DeleteUser);
    }

    private static void ShowUserInfo()
    {
        string id = GetSelectedUserID();
        if (id == " ")
            return;
        Dialog.RequestUserInfo(id);
    }

    private static void VisiblityPageNumber(int number, bool visible)
    {
        if (number <= instance.PageNumberList.Count)
            instance.PageNumberList[number].SetActive (visible);
    }

    private static void VisibleOnAllPageNumber()
    {
        for (int i = 0; i < 12; i++)
            VisiblityPageNumber(i, true);
    }

    private static void VisibleOffRemainPageNumber(int lastNumber)
    {
        for(int i = lastNumber+1; i<12; i++)
        {
            VisiblityPageNumber(i, false);
        }
    }

    private static void ChangePageNumber(int number, int changeNumber)
    {
        instance.PageNumberList[number].GetComponentInChildren<Text>().text = ""+changeNumber;
    }

    private static void ChangePageNumberSet(int startNumber)
    {
        for(int i = 1; i<11; i++)
        {
            ChangePageNumber(i, startNumber++);
        }
    }

    private static void VisiblityPageLeftArrow(bool visible)
    {
        instance.PageNumberList[0].SetActive(visible);
    }

    private static void VisiblityPageRightArrow(bool visible)
    {
        instance.PageNumberList[11].SetActive(visible);
    }


    public static void SettingPage(int nowPage, int totalNum)
    {
        instance.nowPage = nowPage;
        int startNum = CalcPageStartNumber();
        VisibleOnAllPageNumber();
        ChangePageNumberSet(startNum);
        if(startNum == 1)
        {
            VisiblityPageLeftArrow(false);
        }
        //같은 페이지다.
        if (totalNum - startNum < 10)
        {
            VisiblityPageRightArrow(false);
            VisibleOffRemainPageNumber(totalNum-startNum+1);
        }
    }

    public static int MAX
    {
        get
        {
            return instance.max;
        }
        
    }


    private static void ClickPageButton(int index)
    {
        if(index == 0)
        {
            ClickLeftArrowButton();
        }
        else if(index == 11)
        {
            ClickRightArrowButton();
        }
        else
        {
            SocketManager.Send("2#!1#!" +GetSelectedPartner()+"#!" + MAX + "#!" + (CalcPageStartNumber() + (index - 1)));
        }
    }

    private static int CalcNextPage()
    {
        return CalcPageStartNumber() + 10;
    }

    private static int CalcPrevPage()
    {
        return CalcPageStartNumber() - 1;
    }

    private static int CalcPageStartNumber()
    {
        if(instance.nowPage % 10 != 0)
            return (instance.nowPage / 10) * 10 + 1;
        else
            return ((instance.nowPage / 10)-1) * 10+1;
    }

    private static void ClickRightArrowButton()
    {
        SocketManager.Send("2#!1#!"+GetSelectedPartner()+"#!" + MAX + "#!" + CalcNextPage());
    }

    private static void ClickLeftArrowButton()
    {
        SocketManager.Send("2#!1#!" + GetSelectedPartner() + "#!" + MAX + "#!" + CalcPrevPage());
    }

}
