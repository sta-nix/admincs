﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


public class Question : MonoBehaviour
{

    private static Question instance;

    public Transform parent;
    public Transform dummyQchat;
    public InputField inputID;

    public GameObject QChatPrefab;
    public GameObject QListItemPrefab;

    private List<string> qChatIDList = new List<string>();

    void Awake()
    {
        instance = this;    
    }

    public static void DeleteAll()
    {
        int count = Parent.childCount;
        while(Parent.childCount != 1)
        {
            DestroyImmediate(Parent.GetChild(1).gameObject);
        }
    }

    //questioner가 id랑 똑같은데 admin의 응답도 담다보니까.. questioner에는 admin이 들어갈수도있다.
    public static void AddQChatItem(string id, string questioner, string text)
    {
        //Q챗들 탐색하여(id)
        Transform qChat = GetQChat(id);
        if (qChat == instance.dummyQchat)
        {
            qChat = Instantiate(instance.QChatPrefab, Parent).transform;
            SetChaterName(qChat, id);
            SetOnClick(qChat);
            if(!QChatIDList.Exists(str=> str == id))
            {
                QChatIDList.Add(id);
            }
        }
        //QItem을 생성
        AddItem(qChat.Find("Scroll View").Find("View").Find("QList"), questioner, text);
        Alarm.Set(Alarm.MooneNumber, QChatIDList.Count);

    }

    public static List<string> GetActiveQuestionIDList()
    {
        /*
        List<string> ret = new List<string>();
        int count = Parent.childCount;
        for(int i = 0; i<count; i++)
        {
            ret.Add(Parent.GetChild(i).Find("ChaterName").GetComponent<Text>().text);
        }
        */
        return QChatIDList;
    }

    private static void AddItem(Transform list, string questioner, string text)
    {
        GameObject item = Instantiate(instance.QListItemPrefab, list);
        SetItem(item.transform, questioner, text);
    }

    private static void SetChaterName(Transform qChat,string name)
    {
        qChat.Find("ChaterName").GetComponent<Text>().text = name;
    }

    //id이름의 Qchat을 가져옴.
    private static Transform GetQChat(string id)
    {
        int count = Parent.childCount;
        for(int i = 1; i<count; i++)
        {
            if (Parent.GetChild(i).Find("ChaterName").GetComponent<Text>().text == id)
                return Parent.GetChild(i);
        }
        return instance.dummyQchat;
    }

    private static void SetItem(Transform item, string questioner, string text)
    {
        item.Find("Id").GetComponent<Text>().text = questioner;
        item.Find("ItemContent").GetComponent<Text>().text = text;
    }

    //QChat 만들어질때 호출
    public static void SetOnClick(Transform qChat)
    {
        
        qChat.Find("QChatDelete").GetComponent<Button>().onClick.AddListener(() => {
            ClickDeleteQChat(qChat);
            
        });

        qChat.Find("Button").GetComponent<Button>().onClick.AddListener(() => {
            ClickSendAnswer(qChat);
        });
    }

    public static void ForceScrollDown(string id)
    {
        Transform qChat = GetQChat(id);
        qChat.Find("Scroll View").GetComponent<ScrollRect>().verticalNormalizedPosition = -1f;
        Canvas.ForceUpdateCanvases();
        
    }

    #region Events

    public void ClickAddQChat()
    {
        //서버에서 id에 해당하는 목록요청
        List<string> list =GetActiveQuestionIDList();
        string msg = "2#!25#!" + (list.Count+1) + "#!";
        for (int i = 0; i < list.Count; i++)
        {
            msg += list[i] + "#!";
        }
        msg += InputID.text;
        SocketManager.Send(msg);
    }

    public static void ClickSendAnswer(Transform qChat)
    {

        string sendContent = Set1000Char(qChat.Find("InputField").GetComponent<InputField>().text);

        SocketManager.Send("3#!5#!"+qChat.Find("ChaterName").GetComponent<Text>().text +"#!"+ sendContent);
    }

    public static void ClickDeleteQChat(Transform qChat)
    {
        qChat.gameObject.SetActive(false);
        Destroy(qChat.gameObject);
        QChatIDList.Remove(qChat.Find("ChaterName").GetComponent<Text>().text);
        Alarm.Add(Alarm.MooneNumber, -1);
    }

    #endregion


    private static string Set1000Char(string str)
    {
        if (str.Length > 1000)
        {
            return str.Substring(0, 1000);
        }
        return str;
    }


    #region Setter Getter
    public static Transform Parent
    {
        get
        {
            return instance.parent;
        }

        set
        {
            instance.parent = value;
        }
    }

    public static InputField InputID
    {
        get
        {
            return instance.inputID;
        }

        set
        {
            instance.inputID = value;
        }
    }

    private static List<string> QChatIDList
    {
        get
        {
            return instance.qChatIDList;
        }

        set
        {
            instance.qChatIDList = value;
        }
    }
    #endregion
}