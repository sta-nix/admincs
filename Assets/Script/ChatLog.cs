﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatLog : MonoBehaviour
{

    private static ChatLog instance;

    

    public GameObject ChatTable;
    public GameObject RowPrefab;
    public GameObject Unused;
    public ScrollRect chatScrollrect;
    public int MAXINDEX;

    private Queue<GameObject> ChatRowQueue = new Queue<GameObject>();
    private Queue<GameObject> UnusedRowQueue = new Queue<GameObject>();

    void Awake()
    {
        instance = this;
    }
    
    public static void AddChatRow(string type, string id, string content, string which)
    {
        if (RowQueue.Count > instance.MAXINDEX)
        {
            GameObject unused = RowQueue.Dequeue();
            unused.transform.SetParent(UnusedTransform);
            SetRowItem(unused, type, id, content, which);
            unused.transform.SetParent(ChatLogTable.transform);
            RowQueue.Enqueue(unused);
        }
        else
        {
            if (UnusedQueue.Count > 0)
            {
                GameObject used = UnusedQueue.Dequeue();
                used.transform.SetParent(ChatLogTable.transform);
                SetRowItem(used, type, id, content, which);
                RowQueue.Enqueue(used);
            }
            else
            {
                GameObject used = Instantiate(instance.RowPrefab, ChatLogTable.transform);
                SetRowItem(used, type, id, content, which);
                RowQueue.Enqueue(used);
            }
        }
        Vector3 chatPos = ChatLogTable.GetComponent<RectTransform>().position;
        
    }

    public static void ForceScrollDown()
    {
        ChatScrollrect.verticalScrollbar.value = -0.5F;
        Canvas.ForceUpdateCanvases();
    }

    public static void DeleteChatRow()
    {
        if(RowQueue.Count > 0)
        {
            GameObject unused = RowQueue.Dequeue();
            unused.transform.SetParent(UnusedTransform);
            UnusedQueue.Enqueue(unused);
        }
    }

    public static void DeleteAllRow()
    {
        while(RowQueue.Count > 0)
        {
            DeleteChatRow();
        }
    }

    private static void SetRowItem(GameObject row,string type, string id, string content, string which)
    {
        Color c = new Color(0.5f,0.5f,0.5f);
        switch(type)
        {
            case "0":
                type = "공지";
                break;
            case "1":
                type = "봇";
                c = Color.blue;
                break;
            case "2":
                type = "유저";
                c = Color.red;
                break;
        }
        SetItemText(row, "Type", type,c);
        SetItemText(row, "Id", id);
        SetItemText(row, "Content", content);
        SetItemText(row, "Which", which);
    }

    private static void SetItemText(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static void SetItemText(GameObject row, string name, string value, Color color)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
        row.transform.Find(name).GetComponent<Text>().color = color;
    }


    private static void SetItemInputField(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<InputField>().text = value;
    }

    private static Queue<GameObject> RowQueue
    {
       get { return instance.ChatRowQueue; }
    }

    private static Queue<GameObject> UnusedQueue
    {
        get { return instance.UnusedRowQueue;  }
    }
    
    private static Transform UnusedTransform
    {
        get { return instance.Unused.transform; }
    }

    private static GameObject ChatLogTable
    {
        get { return instance.ChatTable; }
    }

    private static ScrollRect ChatScrollrect
    {
        get
        {
            return instance.chatScrollrect;
        }
    }
}
