﻿using UnityEngine;
using System.Text.RegularExpressions;
using System.IO;

public class SaveLoad : MonoBehaviour {

    public static SaveLoad instance;
    //public TextAsset MecroText;
    //private static string path = "Assets/Resources/saves/mecro.txt";
    private static string path = "mecro.txt";
    //private StreamWriter writer;
    private bool isCreated = false;

    private void Awake()
    {
        instance = this;
        if (!File.Exists(path))
        {
            File.CreateText(path);
            isCreated = true;
        }
    }

    private void Start()
    {
        if(isCreated)
        {
            StreamWriter writer = new StreamWriter(path, false);
            writer.WriteLine("#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!");
            writer.Close();
        }
    }

    public static void SaveMecros()
    {
        StreamWriter writer = new StreamWriter(path, false); 
        string writeTxt = "";

        for (int i = 0; i<20; i++)
        {
            writeTxt+=Mecro.GetMecro(i)+"#!";
        }
        writer.WriteLine(writeTxt);

        writer.Close();
    }

    public static void LoadMecros()
    {
        StreamReader reader = new StreamReader(path);
        //string[] loads = Regex.Split(instance.MecroText.text, "#!");
        string[] loads = Regex.Split(reader.ReadLine(), "#!");
        if (loads.Length>20)
        for(int i = 0; i<20; i++)
        {
            Mecro.SetMecro(i, loads[i]);
        }
    }

}
