﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Withdraw : MonoBehaviour {

    private static Withdraw instance;


    public GameObject Table; //RowGroup
    public GameObject RowPrefab;
    public GameObject TotalRow;

    private List<GameObject> RowList = new List<GameObject>();

    public int selectListIndex;

    void Awake()
    {
        instance = this;
    }

    public static void AddRow(string num, string date, string id,string partner,string bank,string account, string name,string money, string confirm, string process)
    {
        int emptyRow = GetUnusedRowIndex();

        GameObject item;
        if (emptyRow == -1)
        {
            item = Instantiate(instance.RowPrefab, instance.Table.transform);

            instance.RowList.Add(item);
            SetConfirmOnClick(instance.RowList.Count - 1);
            SetProcessOnClick(instance.RowList.Count - 1);
            SetDeleteOnClick(instance.RowList.Count - 1);
        }
        else
        {
            item = instance.RowList[emptyRow];
            SetActiveRowList(emptyRow, true);
        }


        SetItem(item, "Num", "" + num);
        SetItem(item, "Date", "" + date);
        SetItem(item, "Id", "" + id);
        SetItem(item, "Partner", "" + partner);
        SetItem(item, "Bank", "" + bank);
        SetItem(item, "Account", "" + account);
        SetItem(item, "Name", "" + name);
        SetItem(item, "Money", "" + money);
        SetItem(item, "Confirm", "" + confirm);
        SetItem(item, "Processing", "" + process);


    }

    public static void SetTotalRow(int money)
    {
        SetItem(instance.TotalRow, "Money", "" + money);
    }

    public static void DeleteAllRows()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            SetActiveRowList(i, false);
        }
        ClearTotalRow();
    }

    public static string GetSelectedUserID()
    {
        return instance.RowList[instance.selectListIndex].transform.Find("Id").GetComponent<Text>().text;
    }

    /*******************************************************************************************************/

    private static void SetActiveRowList(int index, bool state)
    {
        if (instance.RowList.Count > index)
        {
            instance.RowList[index].SetActive(state);
        }
    }
    private static void ClearTotalRow()
    {
        SetTotalRow(0);
    }

    private static int GetUnusedRowIndex()
    {
        for (int i = 0; i < instance.RowList.Count; i++)
        {
            if (!instance.RowList[i].activeSelf)
                return i;
        }
        return -1;
    }

    private static void SetItem(GameObject row, string name, string value)
    {
        row.transform.Find(name).GetComponent<Text>().text = value;
    }

    private static string GetItemText(int index, string name)
    {
        return instance.RowList[index].transform.Find(name).GetComponent<Text>().text;
    }

    private static void SetConfirmOnClick(int index)
    {
        GetButton("Confirm", index).onClick.AddListener(() => { instance.selectListIndex = index; ConfirmModule(index); });
    }

    private static void SetProcessOnClick(int index)
    {
        GetButton("Processing", index).onClick.AddListener(() => { instance.selectListIndex = index; ProcessingModule(index);  });
    }

    private static void SetDeleteOnClick(int index)
    {
        GetButton("Cancel", index).onClick.AddListener(() => { instance.selectListIndex = index; Delete(index); });
    }

    private static void ConfirmModule(int index)
    {
        //확인했으니 알람하나를 지운다.

        //확인했다고 서버에 전송
        SocketManager.Send("2#!15#!" + GetItemText(index, "Num") + "#!0#!");
        SetItem(instance.RowList[index], "Confirm", "O");
    }

    private static void ProcessingModule(int index)
    {
        //처리 됬다고 보내야지.
        SocketManager.Send("2#!15#!" + GetItemText(index, "Num") + "#!1#!");
        SetItem(instance.RowList[index], "Processing", "O");
        SetItem(instance.RowList[index], "Confirm", "O");
    }

    private static void Delete(int index)
    {
        Dialog.RequestConfirmDialog(Confirm.DeleteWithdraw);

    }

    public static void RequestDeleteNowIndex()
    {
        SocketManager.Send("2#!15#!" + GetItemText(instance.selectListIndex, "Num") + "#!2#!");
    }

    private static Button GetButton(string name, int index)
    {
        return instance.RowList[index].transform.Find(name).GetComponentInChildren<Button>();
    }
}
