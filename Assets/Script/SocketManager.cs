﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class SocketManager : MonoBehaviour
{

    public int port;
    public string ip;

    private static SocketManager instance;

    private Socket clientSocket;

    private List<StateObject> stateObjectList = new List<StateObject>();

    private static ManualResetEvent connectDone = new ManualResetEvent(false);
    private static ManualResetEvent sendDone = new ManualResetEvent(false);
    private static ManualResetEvent receiveDone = new ManualResetEvent(false);

    private static String response = String.Empty;

    private byte[] syncMsg = new byte[1024];

    private void OnApplicationQuit()
    {
        //Send("101");
        clientSocket.Shutdown(SocketShutdown.Both);
    }

    

    void Start()
    {
        instance = this;

        // ManualResetEvent instances signal completion.
        try
        {
            //IPAddress ipAddress = IPAddress.Parse(ip);
            IPAddress[] ipAddress = Dns.GetHostAddresses(ip);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress[0], port);

            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Connect to the remote endpoint.
            clientSocket.BeginConnect(remoteEP,
                new AsyncCallback(ConnectCallback), GetRestStateObject());
            bool isConnected =  connectDone.WaitOne(5000,true);
            if(!isConnected)
            {
                clientSocket.Close();
                throw new ApplicationException("Failed to connect server.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("서버연결에 실패" + e.Message);
        }
        Send("2#!3");
    }

    private void ConnectCallback(IAsyncResult ar)
    {
        try
        {

            clientSocket.EndConnect(ar);
            Receive();
            Console.WriteLine("Socket connected to {0}", clientSocket.RemoteEndPoint.ToString());

            // Signal that the connection has been made.
            connectDone.Set();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }


    public static void Send(String data)
    {
        string eof = "#!<EOF>";
        data += eof;
        // Convert the string data to byte data using ASCII encoding.
        byte[] eofByteData = Encoding.UTF8.GetBytes(eof);
        byte[] byteData = Encoding.UTF8.GetBytes(data);

        int length = byteData.Length;

        if(byteData.Length > 4096)
        {
            length = 4096;
            for(int i = 0; i < eofByteData.Length; i++)
            {
                byteData[4095 - i] = eofByteData[eofByteData.Length-1-i];
            }
        }


        // Begin sending the data to the remote device.
        instance.clientSocket.BeginSend(byteData, 0, length, 0,
            new AsyncCallback(SendCallback), null);
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            sendDone.Set();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        //stateobject 사용해제
        StateObject so = (StateObject)ar.AsyncState;
        so.isUsed = false;
    }


    public void Receive()
    {
        try
        {
            // Create the state object.
            StateObject state = GetRestStateObject();

            // Begin receiving the data from the remote device.
            clientSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReceiveCallback), state);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public string ReceiveSync()
    {
        try
        {
            int byteCount = clientSocket.Receive(syncMsg);
            return Encoding.UTF8.GetString(syncMsg, 0, byteCount);
        }
        catch (Exception e)
        {
            Debug.Log("ReceiveSync Error:"+ e.Message);
        }
        return "error";
    }

    private void ReceiveCallback(IAsyncResult ar)
    {
        String content = String.Empty;
        StateObject state = (StateObject)ar.AsyncState;
        try
        {

            int bytesRead = clientSocket.EndReceive(ar);

            if (bytesRead > 0)
            {
                // 더 많은 데이터가있을 수 있음.
                // 현재까지의 데이터를 저장한다.
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                // EOF를 체크해서 없으면 더 받는다. 있으면 반응한다.
                // 몇바이트 보내는데 쪼개질일이 있겠냐.. 
                content = state.sb.ToString();

                if (content.IndexOf("<EOF>") > -1)
                {
                    MainModule.RecvModule(content);

                    state.sb.Remove(0, state.sb.Length);
                    Receive();
                }
                else
                {
                    clientSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
            }
            else
            {
                if (state.sb.Length > 1)
                {
                    response = state.sb.ToString();
                    Debug.Log(response);
                    //여기에 모듈 연결해주면 끝..
                }
                receiveDone.Set();
            }

        }
        catch (Exception e)
        {
            //Console.WriteLine(e.ToString());
        }
        state.isUsed = false;
        
    }

    private StateObject GetRestStateObject()
    {
        for (int i = 0; i < stateObjectList.Count; i++)
        {
            if (stateObjectList[i].isUsed == false)
            {
                stateObjectList[i].isUsed = true;
                return stateObjectList[i];

            }
        }
        StateObject stateObject = new StateObject();
        stateObjectList.Add(stateObject);
        stateObject.isUsed = true;
        return stateObject;
    }
}

public class StateObject
{
    // Size of receive buffer.
    public const int BufferSize = 32768;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
    public bool isUsed = false;
}