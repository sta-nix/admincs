﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TermPartner : MonoBehaviour {

    private static TermPartner instance;

    public InputField StartDate, EndDate;
    public Dropdown PresetToday, PresetMonth, Preset15Days ;
    public Dropdown PartnerDropdown;

    public TermPanel NowSelectTermPanel = TermPanel.RevenueList;

    void Awake()
    {
        instance = this;
    }

    public static string GetStartDate()
    {
        return instance.StartDate.text;
    }

    public static string GetEndDate()
    {
        return instance.EndDate.text;
    }

    public static void SetPartnerDropdown(List<string> ids)
    {
        instance.PartnerDropdown.ClearOptions();

        instance.PartnerDropdown.AddOptions(ids);
    }

    public static void InitPresets()
    {
        instance.PresetToday.value = 0;
        instance.PresetMonth.value = 0;
        instance.Preset15Days.value = 0;
    }

    public static void SetSelectedPanel(TermPanel tp)
    {
        instance.NowSelectTermPanel = tp;
    }

    public static void SetDateToday()
    {
        instance.StartDate.text = DateTime.Today.ToString("yyyy-MM-dd");
        instance.EndDate.text = DateTime.Today.ToString("yyyy-MM-dd");
    }

    public void OnPresetToday()
    {
        /*
        if (PresetToday.value == 0)
        {
            return;
        }*/
        PresetMonth.value = 0;
        Preset15Days.value = 0;
        
        
        EndDate.text = DateTime.Today.ToString("yyyy-MM-dd");
        int[] days = new int[6] { 0, 7, 15, 30, 60, 90 };
        string start = DateTime.Today.AddDays(-1 * days[PresetToday.value]).ToString("yyyy-MM-dd");
        StartDate.text = start;
        
    }

    public void OnPresetMonth()
    {
        if (PresetMonth.value == 0)
            return;
        Preset15Days.value = 0;
        PresetToday.value = 0;

        DateTime month = new DateTime( DateTime.Today.Year, DateTime.Today.Month,1);
        StartDate.text = month.AddMonths(-PresetMonth.value + 1).ToString("yyyy-MM-dd");
        EndDate.text = month.AddMonths(-PresetMonth.value + 2).AddDays(-1).ToString("yyyy-MM-dd");
    }

    public void OnPreset15days()
    {
        if(Preset15Days.value == 0)
        {
            return;
        }

        DateTime month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        PresetMonth.value = 0;
        PresetToday.value = 0;
        //12주
        if (Preset15Days.value % 2 == 0)
        {
            StartDate.text = month.AddMonths((-Preset15Days.value + 1)/2).ToString("yyyy-MM-dd");
            EndDate.text = month.AddMonths((-Preset15Days.value + 1) / 2).AddDays(14).ToString("yyyy-MM-dd");

        }
        //34주 홀수
        else
        {
            StartDate.text = month.AddMonths((-Preset15Days.value + 1) / 2).AddDays(15).ToString("yyyy-MM-dd");
            EndDate.text = month.AddMonths((-Preset15Days.value + 3) / 2).AddDays(-1).ToString("yyyy-MM-dd");

        }


    }


//events
    public void Search()
    {
        DateTime startDT, EndDT;
        
        DateTime.TryParse(StartDate.text, out startDT);
        DateTime.TryParse(EndDate.text, out EndDT);

        if (startDT.Year == 1 || EndDT.Year == 1 || NowSelectTermPanel == TermPanel.Virtual)
        {
            return;
        }

        string ret = "2#!"+(int)NowSelectTermPanel+"#!";

        ret += startDT.ToShortDateString() + "#!";
        ret += EndDT.ToShortDateString() + "#!";
        if(PartnerDropdown.value == 0)
        {
            ret += "0#!";
        }
        else
        {
            ret += PartnerDropdown.options[PartnerDropdown.value].text +"#!";
        }

        SocketManager.Send(ret);

    }

}

public enum TermPanel
{
    RevenueList=10, DepositList=12, WithdrawList, Virtual
}