﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TermPrivate : MonoBehaviour {

    private static TermPrivate instance;

    public InputField StartDate, EndDate;
    public Dropdown PresetToday, PresetMonth, Preset15Days ;



    void Awake()
    {
        instance = this;
    }

    public static string GetStartDate()
    {
        return instance.StartDate.text;
    }

    public static string GetEndDate()
    {
        return instance.EndDate.text;
    }
    


    public void OnPresetToday()
    {
        if (PresetToday.value == 0)
        {
            return;
        }
        PresetMonth.value = 0;
        Preset15Days.value = 0;
        
        
        EndDate.text = DateTime.Today.ToString("yyyy-MM-dd");
        int[] days = new int[6] { 1, 7, 15, 30, 60, 90 };
        string start = DateTime.Today.AddDays(-1 * days[PresetToday.value]).ToString("yyyy-MM-dd");
        StartDate.text = start;
        
    }

    public void OnPresetMonth()
    {
        if (PresetMonth.value == 0)
            return;
        Preset15Days.value = 0;
        PresetToday.value = 0;

        DateTime month = new DateTime( DateTime.Today.Year, DateTime.Today.Month,1);
        StartDate.text = month.AddMonths(-PresetMonth.value + 1).ToString("yyyy-MM-dd");
        EndDate.text = month.AddMonths(-PresetMonth.value + 2).AddDays(-1).ToString("yyyy-MM-dd");
    }

    public void OnPreset15days()
    {
        if(Preset15Days.value == 0)
        {
            return;
        }

        DateTime month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        PresetMonth.value = 0;
        PresetToday.value = 0;
        //12주
        if (Preset15Days.value % 2 == 0)
        {
            StartDate.text = month.AddMonths((-Preset15Days.value + 1)/2).ToString("yyyy-MM-dd");
            EndDate.text = month.AddMonths((-Preset15Days.value + 1) / 2).AddDays(14).ToString("yyyy-MM-dd");

        }
        //34주 홀수
        else
        {
            StartDate.text = month.AddMonths((-Preset15Days.value + 1) / 2).AddDays(15).ToString("yyyy-MM-dd");
            EndDate.text = month.AddMonths((-Preset15Days.value + 3) / 2).AddDays(-1).ToString("yyyy-MM-dd");

        }


    }


//events
    public void Search()
    {
        DateTime startDT, EndDT;
        
        DateTime.TryParse(StartDate.text, out startDT);
        DateTime.TryParse(EndDate.text, out EndDT);

        if (startDT.Year == 1 || EndDT.Year == 1 )
        {
            return;
        }

        string ret = "7#!1#!"+PrivatePage.PartnerID+"#!";

        ret += startDT.ToShortDateString() + "#!";
        ret += EndDT.ToShortDateString() + "#!";

        SocketManager.Send(ret);
    }

}
